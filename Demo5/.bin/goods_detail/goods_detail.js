(function() {
	/**
	 * 判断当前是否是小程序环境
	 * 2020-11-24  暂用 api.uiMode模拟
	 * 后期需修正为  api.platform
	 * @returns {boolean}
	 */
	function isMP() {
		return !api.uiMode;
	}

	/**
	 * 统一设置底部tabBar的数字小红点
	 * @param index
	 * @param text
	 */
	function setTabBarBadge(index, text) {
		text += "";
		if (isMP()) {
			if (text === "0") {
				wx.removeTabBarBadge({index: index});
			} else {
				wx.setTabBarBadge({index: index, text: text});
			}
		} else {
			api.setTabBarItemAttr({index: index, badge: {text: text, color: "#FFF"}});
		}
	}

	var GoodsAction = /*@__PURE__*/ (function(Component) {
		function GoodsAction(props) {
			Component.call(this, props);
		}

		if (Component) GoodsAction.__proto__ = Component;
		GoodsAction.prototype = Object.create(Component && Component.prototype);
		GoodsAction.prototype.constructor = GoodsAction;
		GoodsAction.prototype.crash = function() {
			var pageParam = {list: [this.props]};
			api.openWin({
				name: "pending_order",
				url: "../../pages/pending_order/pending_order.stml",
				pageParam: pageParam
			});
		};
		GoodsAction.prototype.addCart = function() {
			this.fire("addCart");
		};
		GoodsAction.prototype.render = function() {
			return apivm.h(
				"safe-area",
				{class: "action-bar"},

				apivm.h(
					"view",
					{class: "action-btn btn-add-cart", onClick: this.addCart},
					apivm.h("text", {class: "btn-text"}, "加入购物车")
				),
				apivm.h(
					"view",
					{class: "action-btn btn-crash", onClick: this.crash},
					apivm.h("text", {class: "btn-text"}, "结算")
				)
			);
		};

		return GoodsAction;
	})(Component);
	GoodsAction.css = {
		".action-bar": {flexDirection: "row", background: "#fff"},
		".btn-add-cart": {background: "#FFC54A", height: "48px"},
		".btn-crash": {background: "#FE5760", height: "48px"},
		".btn-text": {color: "#fff", fontSize: "15px"},
		".action-btn": {flex: "1", alignItems: "center", justifyContent: "center"}
	};
	apivm.define("goods-action", GoodsAction);

	var GoodsCounter = /*@__PURE__*/ (function(Component) {
		function GoodsCounter(props) {
			Component.call(this, props);
		}

		if (Component) GoodsCounter.__proto__ = Component;
		GoodsCounter.prototype = Object.create(Component && Component.prototype);
		GoodsCounter.prototype.constructor = GoodsCounter;
		GoodsCounter.prototype.countChange = function(change) {
			if (this.props.count + change === 0) {
				return api.toast({
					msg: "不能再减少了\n可在购物车编辑模式下移除",
					location: "middle"
				});
			}
			this.fire("CountChange", {
				change: change,
				props: this.props
			});
		};
		GoodsCounter.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"view",
				{class: "flex-h"},
				apivm.h("img", {
					class: "num-btn num-decrease",
					src: "../../image/icon/icon-cart-num-decrease.png",
					onClick: function() {
						return this$1.countChange(-1);
					}
				}),
				apivm.h("text", {class: "num-text"}, this.props.count),
				apivm.h("img", {
					class: "num-btn num-increase",
					src: "../../image/icon/icon-cart-num-increase.png",
					onClick: function() {
						return this$1.countChange(1);
					}
				})
			);
		};

		return GoodsCounter;
	})(Component);
	GoodsCounter.css = {
		".flex-h": {
			WebkitBoxOrient: "horizontal",
			WebkitFlexFlow: "row",
			flexFlow: "row"
		},
		".num-btn": {width: "22px", height: "22px", backgroundSize: "22px 22px"},
		".num-text": {
			padding: "0 8px",
			fontSize: "14px",
			color: "#333",
			marginTop: "2px",
			minWidth: "28px",
			textAlign: "center"
		}
	};
	apivm.define("goods-counter", GoodsCounter);

	var GoodsDetail = /*@__PURE__*/ (function(Component) {
		function GoodsDetail(props) {
			Component.call(this, props);
			this.data = {
				goods: false,
				count: 1,
				cartData: false,
				safeAreaTop: 0
			};
			this.compute = {
				price: function() {
					return Number(this.data.goods.curt_price)
						.toFixed(2)
						.split(".");
				}
			};
		}

		if (Component) GoodsDetail.__proto__ = Component;
		GoodsDetail.prototype = Object.create(Component && Component.prototype);
		GoodsDetail.prototype.constructor = GoodsDetail;
		GoodsDetail.prototype.apiready = function() {
			this.data.goods = api.pageParam.item.togoods;
			this.data.safeAreaTop = api.safeArea ? api.safeArea.top : 0;
			var cartList = api.getPrefs({sync: true, key: "CART-DATA"});
			if (cartList) {
				cartList = JSON.parse(cartList);
				this.data.cartData = cartList[this.data.goods.id];
				if (this.data.cartData) {
					this.data.count = this.data.cartData.count;
				}
			}
		};
		GoodsDetail.prototype.countChange = function(e) {
			this.data.count += e.detail.change;
		};
		GoodsDetail.prototype.closeWin = function() {
			api.closeWin();
		};
		GoodsDetail.prototype.addCart = function() {
			var cartList = api.getPrefs({sync: true, key: "CART-DATA"}) || "{}";
			cartList = JSON.parse(cartList);
			cartList[this.data.goods.id] = {
				goods: this.data.goods,
				count: this.data.count
			};
			api.setPrefs({key: "CART-DATA", value: cartList});
			api.toast({
				msg: "成功加入" + this.data.count + "个到购物车",
				location: "middle"
			});
			setTabBarBadge(2, Object.keys(cartList).length);
		};
		GoodsDetail.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"view",
				{class: "page flex"},
				apivm.h("img", {
					class: "goods-pic",
					mode: "aspectFill",
					src: this.data.goods.pic_main,
					alt: ""
				}),
				apivm.h(
					"view",
					{
						class: "back-btn",
						style: "top:" + this.data.safeAreaTop + "px",
						onClick: this.closeWin
					},
					apivm.h("img", {
						class: "back-btn-img",
						src: "../../image/icon/icon-goods-detail-back-btn.png",
						alt: ""
					})
				),
				apivm.h(
					"view",
					{class: "goods-info"},
					apivm.h("text", {class: "goods-name"}, this.data.goods.name),
					apivm.h("text", {class: "goods-desc"}, this.data.goods.description)
				),
				apivm.h(
					"view",
					{class: "price-count flex-h flex-1"},
					apivm.h(
						"view",
						{class: "goods-price-wrap flex-h"},
						apivm.h("text", {class: "goods-price-signal"}, "¥"),
						apivm.h("text", {class: "goods-price"}, this.price[0]),
						apivm.h("text", {class: "goods-price-signal"}, ".", this.price[1])
					),
					apivm.h(
						"view",
						{class: "num-count"},
						apivm.h(GoodsCounter, {
							onCountChange: this.countChange.bind(this),
							count: this.data.count
						})
					)
				),
				apivm.h(GoodsAction, {
					goods: this.data.goods,
					count: this.data.count,
					onAddCart: function(event) {
						if (this$1.addCart) {
							this$1.addCart(event);
						} else {
							addCart(event);
						}
					}
				})
			);
		};

		return GoodsDetail;
	})(Component);
	GoodsDetail.css = {
		".page": {height: "100%"},
		".goods-pic": {width: "100%", height: "375px"},
		".goods-price-wrap": {flex: "1"},
		".goods-info": {padding: "16px 15px", borderBottom: "1px solid #EEEEEE"},
		".goods-name": {fontSize: "18px", fontWeight: "500", color: "#333333"},
		".goods-desc": {fontSize: "15px", color: "#666666", marginTop: "5px"},
		".price-count": {padding: "10px 15px", flex: "1", alignItems: "flex-start"},
		".goods-price-signal": {fontSize: "20px", color: "#FE5760", marginTop: "8px"},
		".goods-price": {fontSize: "28px", color: "#FE5760", fontWeight: "bold"},
		".num-count": {marginTop: "6px"},
		".back-btn": {
			position: "absolute",
			left: "15px",
			width: "32px",
			height: "32px",
			zIndex: "999"
		},
		".back-btn-img": {width: "32px", height: "32px"}
	};
	GoodsDetail.csslink = ["../../css/app.css"];
	apivm.define("goods_detail", GoodsDetail);
	apivm.render(apivm.h("goods_detail", null), "body");
})();
