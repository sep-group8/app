(function() {
	/**
	 * 判断当前是否是小程序环境
	 * 2020-11-24  暂用 api.uiMode模拟
	 * 后期需修正为  api.platform
	 * @returns {boolean}
	 */
	function isMP() {
		return !api.uiMode;
	}

	/**
	 * 统一设置底部tabBar的数字小红点
	 * @param index
	 * @param text
	 */
	function setTabBarBadge(index, text) {
		text += "";
		if (isMP()) {
			if (text === "0") {
				wx.removeTabBarBadge({index: index});
			} else {
				wx.setTabBarBadge({index: index, text: text});
			}
		} else {
			api.setTabBarItemAttr({index: index, badge: {text: text, color: "#FFF"}});
		}
	}

	var GoodsAction = /*@__PURE__*/ (function(Component) {
		function GoodsAction(props) {
			Component.call(this, props);
		}

		if (Component) GoodsAction.__proto__ = Component;
		GoodsAction.prototype = Object.create(Component && Component.prototype);
		GoodsAction.prototype.constructor = GoodsAction;
		GoodsAction.prototype.crash = function() {
			var pageParam = {list: [this.props]};
			api.openWin({
				name: "pending_order",
				url: "../../pages/pending_order/pending_order.stml",
				pageParam: pageParam
			});
		};
		GoodsAction.prototype.addCart = function() {
			this.fire("addCart");
		};
		GoodsAction.prototype.render = function() {
			return apivm.h(
				"safe-area",
				{class: "action-bar"},

				apivm.h(
					"view",
					{class: "action-btn btn-add-cart", onClick: this.addCart},
					apivm.h("text", {class: "btn-text"}, "加入购物车")
				),
				apivm.h(
					"view",
					{class: "action-btn btn-crash", onClick: this.crash},
					apivm.h("text", {class: "btn-text"}, "结算")
				)
			);
		};

		return GoodsAction;
	})(Component);
	GoodsAction.css = {
		".action-bar": {flexDirection: "row", background: "#fff"},
		".btn-add-cart": {background: "#FFC54A", height: "48px"},
		".btn-crash": {background: "#FE5760", height: "48px"},
		".btn-text": {color: "#fff", fontSize: "15px"},
		".action-btn": {flex: "1", alignItems: "center", justifyContent: "center"}
	};
	apivm.define("goods-action", GoodsAction);

	var GoodsCounter = /*@__PURE__*/ (function(Component) {
		function GoodsCounter(props) {
			Component.call(this, props);
		}

		if (Component) GoodsCounter.__proto__ = Component;
		GoodsCounter.prototype = Object.create(Component && Component.prototype);
		GoodsCounter.prototype.constructor = GoodsCounter;
		GoodsCounter.prototype.countChange = function(change) {
			if (this.props.count + change === 0) {
				return api.toast({
					msg: "不能再减少了\n可在购物车编辑模式下移除",
					location: "middle"
				});
			}
			this.fire("CountChange", {
				change: change,
				props: this.props
			});
		};
		GoodsCounter.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"view",
				{class: "flex-h"},
				apivm.h("img", {
					class: "num-btn num-decrease",
					src: "../../image/icon/icon-cart-num-decrease.png",
					onClick: function() {
						return this$1.countChange(-1);
					}
				}),
				apivm.h("text", {class: "num-text"}, this.props.count),
				apivm.h("img", {
					class: "num-btn num-increase",
					src: "../../image/icon/icon-cart-num-increase.png",
					onClick: function() {
						return this$1.countChange(1);
					}
				})
			);
		};

		return GoodsCounter;
	})(Component);
	GoodsCounter.css = {
		".flex-h": {
			WebkitBoxOrient: "horizontal",
			WebkitFlexFlow: "row",
			flexFlow: "row"
		},
		".num-btn": {width: "22px", height: "22px", backgroundSize: "22px 22px"},
		".num-text": {
			padding: "0 8px",
			fontSize: "14px",
			color: "#333",
			marginTop: "2px",
			minWidth: "28px",
			textAlign: "center"
		}
	};
	apivm.define("goods-counter", GoodsCounter);

	var GoodsAdd = /*@__PURE__*/ (function(Component) {
		function GoodsAdd(props) {
			Component.call(this, props);
			this.data = {
				background: "rgba(0,0,0,0)",
				goods: false,
				goodsCardBottom: 375,
				count: 1
			};
		}

		if (Component) GoodsAdd.__proto__ = Component;
		GoodsAdd.prototype = Object.create(Component && Component.prototype);
		GoodsAdd.prototype.constructor = GoodsAdd;
		GoodsAdd.prototype.installed = function() {
			var this$1 = this;
			this.data.goods = this.props.goods ? this.props.goods : api.pageParam.goods;
			console.log(this.data.goods);
			setTimeout(function() {
				this$1.data.background = "rgba(0,0,0,.5)";
				this$1.data.goodsCardBottom = 0;
			}, 11);
			var cartList = api.getPrefs({sync: true, key: "CART-DATA"});
			if (cartList) {
				cartList = JSON.parse(cartList);
				this.data.cartData = cartList[this.data.goods.id];
				if (this.data.cartData) {
					this.data.count = this.data.cartData.count;
				}
			}
		};
		GoodsAdd.prototype.closeThis = function() {
			this.data.background = "rgba(0,0,0,0)";
			this.data.goodsCardBottom = 375;
			setTimeout(function() {
				if (isMP()) {
					api.sendEvent({
						name: "CLOSE-GOODS-ADD"
					});
				} else {
					api.closeFrame();
				}
			}, 300);
		};
		GoodsAdd.prototype.countChange = function(e) {
			this.data.count += e.detail.change;
		};
		GoodsAdd.prototype.addCart = function() {
			var cartList = api.getPrefs({sync: true, key: "CART-DATA"}) || "{}";
			cartList = JSON.parse(cartList);
			cartList[this.data.goods.id] = {
				goods: this.data.goods,
				count: this.data.count
			};

			api.setPrefs({
				key: "CART-DATA",
				value: cartList
			});

			setTabBarBadge(2, Object.keys(cartList).length);
			api.toast({
				msg: "成功加入" + this.data.count + "个到购物车",
				location: "middle"
			});

			this.closeThis();
		};
		GoodsAdd.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"view",
				{class: "page", style: "background:" + this.data.background},
				apivm.h("view", {class: "mask", onClick: this.closeThis}),
				apivm.h(
					"view",
					{
						class: "move",
						style: "transform:translateY(" + this.data.goodsCardBottom + "px)"
					},
					apivm.h(
						"view",
						{class: "goods-card"},
						apivm.h("img", {
							class: "goods-cover",
							mode: "aspectFill",
							src: this.data.goods.pic_main,
							alt: ""
						}),
						apivm.h(
							"view",
							{class: "goods-info"},
							apivm.h("text", {class: "goods-title"}, this.data.goods.name),
							apivm.h(
								"view",
								{class: "goods-footer"},
								apivm.h("text", {class: "goods-price-signal"}, "¥"),
								apivm.h("text", {class: "goods-price"}, this.data.goods.curt_price),
								apivm.h(GoodsCounter, {
									onCountChange: this.countChange.bind(this),
									count: this.data.count
								})
							)
						)
					),

					apivm.h(GoodsAction, {
						goods: this.data.goods,
						count: this.data.count,
						onAddCart: function(event) {
							if (this$1.addCart) {
								this$1.addCart(event);
							} else {
								addCart(event);
							}
						}
					})
				)
			);
		};

		return GoodsAdd;
	})(Component);
	GoodsAdd.css = {
		".page": {height: "100%", display: "flex", transition: "all 0.5s"},
		".mask": {flex: "1"},
		".move": {
			position: "relative",
			transition: "all 0.3s",
			transform: "translateY(375px)"
		},
		".goods-card": {
			background: "#fff",
			borderRadius: "10px 10px 0 0",
			padding: "30px 15px",
			flexFlow: "row"
		},
		".goods-cover": {width: "70px", height: "70px"},
		".goods-info": {marginLeft: "10px", flex: "1"},
		".goods-title": {
			fontSize: "14px",
			color: "#333",
			fontWeight: "bold",
			flex: "1"
		},
		".goods-footer": {flexDirection: "row"},
		".goods-price-signal": {fontSize: "12px", color: "#FE5760", marginTop: "3px"},
		".goods-price": {fontSize: "15px", color: "#FE5760", flex: "1"}
	};
	apivm.define("goods-add", GoodsAdd);
	apivm.render(apivm.h("goods-add", null), "body");
})();
