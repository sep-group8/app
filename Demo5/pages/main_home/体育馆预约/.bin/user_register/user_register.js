(function() {
	var UserRegister = /*@__PURE__*/ (function(Component) {
		function UserRegister(props) {
			Component.call(this, props);
			this.data = {
				username: "",
				password: "",
				chkpassword: ""
			};
		}

		if (Component) UserRegister.__proto__ = Component;
		UserRegister.prototype = Object.create(Component && Component.prototype);
		UserRegister.prototype.constructor = UserRegister;
		UserRegister.prototype.apiready = function() {};
		UserRegister.prototype.get_username = function(e) {
			var _this = this;
			_this.data.username = e.detail.value;
		};
		UserRegister.prototype.get_password = function(e) {
			var _this = this;
			_this.data.password = e.detail.value;
		};
		UserRegister.prototype.get_chkpassword = function(e) {
			var _this = this;
			_this.data.chkpassword = e.detail.value;
		};
		UserRegister.prototype.doSubmit = function() {
			var _this = this;
			var username = _this.data.username;
			var password = _this.data.password;
			if (_this.data.password !== _this.data.chkpassword) {
				api.toast({
					msg: "密码不一致"
				});
			} else {
				api.ajax(
					{
						url:
							"https://a6105722061383-dev.apicloud-saas.com/api/c_users/user_register",
						method: "post",
						headers: {
							"Content-Type": "application/json;charset=utf-8"
						},

						data: {
							body: {
								username: username,
								password: password
							}
						}
					},

					function(ret, err) {
						//正常
						console.log(JSON.stringify(ret));
						if (ret.statusCode == 200) {
							api.toast({
								msg: ret.message,
								duration: 2000,
								location: "bottom"
							});

							setTimeout(function(_) {
								_this.doBack();
							}, 2000);
						} else {
							api.toast({
								msg: ret.message,
								duration: 2000,
								location: "bottom"
							});

							return;
						}
						//异常
						if (err) {
							api.toast({
								msg: "网络异常，请稍后重试",
								duration: 2000,
								location: "bottom"
							});

							return;
						}
					}
				);
			}
		};
		UserRegister.prototype.doBack = function() {
			api.closeWin();
		};
		UserRegister.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"scroll-view",
				{class: "page", "scroll-y": true},
				apivm.h(
					"view",
					{class: "reg"},
					apivm.h("text", {class: "reg-txt"}, "新用户注册")
				),
				apivm.h(
					"view",
					{class: "reg_form"},
					apivm.h(
						"view",
						{class: "mobile"},
						apivm.h(
							"view",
							{class: "weui-label"},
							apivm.h("text", {class: "weui-label-text"}, "登录用户：")
						),
						apivm.h("input", {
							type: "text",
							class: "weui-input",
							placeholder: "请输入登录用户名",
							"placeholder-style": "color:#fff",
							"adjust-position": "true",
							oninput: function(event) {
								if (this$1.get_username) {
									this$1.get_username(event);
								} else {
									get_username(event);
								}
							}
						})
					),
					apivm.h(
						"view",
						{class: "password"},
						apivm.h(
							"view",
							{class: "weui-label"},
							apivm.h("text", {class: "weui-label-text"}, "登录密码：")
						),
						apivm.h("input", {
							type: "password",
							class: "weui-input",
							placeholder: "请输入登录密码",
							"placeholder-style": "color:#fff",
							"adjust-position": "true",
							oninput: function(event) {
								if (this$1.get_password) {
									this$1.get_password(event);
								} else {
									get_password(event);
								}
							}
						})
					),
					apivm.h(
						"view",
						{class: "password"},
						apivm.h(
							"view",
							{class: "weui-label"},
							apivm.h("text", {class: "weui-label-text"}, "确认密码：")
						),
						apivm.h("input", {
							type: "password",
							class: "weui-input",
							placeholder: "请输入确认登录密码",
							"placeholder-style": "color:#fff",
							"adjust-position": "true",
							oninput: function(event) {
								if (this$1.get_chkpassword) {
									this$1.get_chkpassword(event);
								} else {
									get_chkpassword(event);
								}
							}
						})
					),
					apivm.h(
						"view",
						{class: "reg-btn", onClick: this.doSubmit},
						apivm.h("text", {class: "reg-btn-txt"}, "立即注册")
					),
					apivm.h(
						"view",
						{class: "back-btn", onClick: this.doBack},
						apivm.h("text", {class: "back-btn-txt"}, "返回登录")
					)
				)
			);
		};

		return UserRegister;
	})(Component);
	UserRegister.css = {
		".page": {
			backgroundImage: 'url("../../image/bg/bg_body.png")',
			backgroundRepeat: "no-repeat",
			backgroundSize: "100%",
			backgroundPosition: "center top",
			backgroundColor: "#5caaff",
			width: "100%",
			height: "100%"
		},
		".reg": {width: "100%", height: "50px", marginTop: "30%"},
		".reg-txt": {
			textAlign: "left",
			paddingLeft: "25px",
			fontSize: "30px",
			color: "#fff"
		},
		".reg_form": {
			width: "80%",
			height: "auto",
			margin: "10% auto 0",
			boxSizing: "border-box"
		},
		input: {border: "none", background: "transparent"},
		".p-color": {color: "#fff"},
		".weui-input": {
			width: "auto",
			height: "auto",
			color: "#fff",
			fontSize: "16px"
		},
		".weui-label": {width: "auto", height: "auto", marginBottom: "10px"},
		".weui-label-text": {color: "#66beff", fontSize: "16px"},
		".mobile": {
			width: "100%",
			height: "auto",
			marginBottom: "10px",
			padding: "0 0 8px",
			boxSizing: "border-box",
			borderBottom: "1px solid #66beff"
		},
		".password": {
			width: "100%",
			height: "auto",
			marginBottom: "10px",
			padding: "0 0 8px",
			boxSizing: "border-box",
			borderBottom: "1px solid #66beff"
		},
		".reg-btn": {
			display: "block",
			width: "100%",
			height: "40px",
			boxSizing: "border-box",
			borderRadius: "20px",
			background: "#fff",
			margin: "20px auto",
			textAlign: "center"
		},
		".reg-btn-txt": {
			lineHeight: "40px",
			fontSize: "20px",
			fontWeight: "bolder",
			color: "#66beff"
		},
		".back-btn": {
			display: "block",
			width: "100%",
			height: "40px",
			boxSizing: "border-box",
			borderRadius: "20px",
			background: "#66beff",
			margin: "0 auto",
			textAlign: "center"
		},
		".back-btn-txt": {
			lineHeight: "40px",
			fontSize: "20px",
			fontWeight: "bolder",
			color: "#fff"
		}
	};
	UserRegister.csslink = ["../../css/app.css"];
	apivm.define("user_register", UserRegister);
	apivm.render(apivm.h("user_register", null), "body");
})();
