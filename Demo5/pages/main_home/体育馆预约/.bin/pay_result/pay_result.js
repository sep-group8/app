(function() {
	/**
	 * 判断当前是否是小程序环境
	 * 2020-11-24  暂用 api.uiMode模拟
	 * 后期需修正为  api.platform
	 * @returns {boolean}
	 */
	function isMP() {
		return !api.uiMode;
	}

	var tabFrames = [
		{
			name: "home",
			url: "/pages/main_home/main_home",
			title: "首页"
		},
		{
			name: "doc",
			url: "/pages/main_menu/main_menu",
			title: "场馆"
		},
		{
			name: "about",
			url: "/pages/main_cart/main_cart",
			title: "购物车"
		},
		{
			name: "about",
			url: "/pages/main_user/main_user",
			title: "我的"
		}
	];

	/**
	 * 统一设置tabBar的index 解决小程序端无法在TabBar中处理切换事件的问题
	 * @param index 目标索引值
	 * @param scroll 是否开启滚动过度
	 */
	function setTabBarIndex(index, scroll) {
		if (scroll === void 0) {
			scroll = true;
		}
		if (isMP()) {
			var pageStack = getCurrentPages();

			if (pageStack.length === 1) {
				console.log(tabFrames[index]);

				wx.switchTab(tabFrames[index]);
			} else {
				console.log("二级页面切换待处理");
			}
		} else {
			api.sendEvent({
				name: "SET-TAB-INDEX",
				extra: {
					index: index,
					scroll: scroll
				}
			});
		}
	}

	var PayResult = /*@__PURE__*/ (function(Component) {
		function PayResult(props) {
			Component.call(this, props);
		}

		if (Component) PayResult.__proto__ = Component;
		PayResult.prototype = Object.create(Component && Component.prototype);
		PayResult.prototype.constructor = PayResult;
		PayResult.prototype.next = function(index) {
			setTabBarIndex(index);
			setTimeout(function(_) {
				api.closeToWin({
					name: "root"
				});
			}, 300);
		};
		PayResult.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"view",
				{class: "page flex flex-center"},

				apivm.h("img", {
					class: "icon",
					src: "../../image/icon/icon-pay-result-success.png",
					alt: ""
				}),
				apivm.h("text", {class: "tips-text"}, "订单支付成功"),

				apivm.h(
					"view",
					{class: "btn-box flex-h flex-center"},
					apivm.h(
						"view",
						{
							class: "btn btn-main",
							onClick: function() {
								return this$1.next(1);
							}
						},
						apivm.h("text", {class: "btn-text btn-text-main"}, "继续选购")
					),
					apivm.h(
						"view",
						{
							class: "btn btn-sub",
							onClick: function() {
								return this$1.next(3);
							}
						},
						apivm.h("text", {class: "btn-text btn-text-sub"}, "查看订单")
					)
				)
			);
		};

		return PayResult;
	})(Component);
	PayResult.css = {
		".page": {height: "100%"},
		".icon": {width: "100px", height: "100px"},
		".tips-text": {fontSize: "18px", color: "#333", margin: "21px"},
		".btn-box": {width: "100%", margin: "105px 0"},
		".btn": {width: "42%", borderRadius: "4px"},
		".btn-main": {background: "#FE5760"},
		".btn-sub": {
			background: "#FFF",
			marginLeft: "10px",
			border: "1px solid #FE5760"
		},
		".btn-text": {
			color: "#FFFFFF",
			textAlign: "center",
			height: "44px",
			lineHeight: "44px",
			fontSize: "15px"
		},
		".btn-text-main": {color: "#FFF"},
		".btn-text-sub": {color: "#FE5760"}
	};
	PayResult.csslink = ["../../css/app.css"];
	apivm.define("pay_result", PayResult);
	apivm.render(apivm.h("pay_result", null), "body");
})();
