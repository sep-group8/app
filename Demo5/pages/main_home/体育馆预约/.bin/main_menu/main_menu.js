(function() {
	/**
	 * 2020-11-24  暂用 api.uiMode模拟
	 * 后期需修正为  api.platform
	 * @returns {boolean}
	 */
	function isMP() {
		return !api.uiMode;
	}

	/**
	 * 统一设置底部tabBar的数字小红点
	 * @param index
	 * @param text
	 */
	function setTabBarBadge(index, text) {
		text += "";
		if (isMP()) {
			if (text === "0") {
				wx.removeTabBarBadge({index: index});
			} else {
				wx.setTabBarBadge({index: index, text: text});
			}
		} else {
			api.setTabBarItemAttr({index: index, badge: {text: text, color: "#FFF"}});
		}
	}

	function ownKeys(object, enumerableOnly) {
		var keys = Object.keys(object);

		if (Object.getOwnPropertySymbols) {
			var symbols = Object.getOwnPropertySymbols(object);

			if (enumerableOnly) {
				symbols = symbols.filter(function(sym) {
					return Object.getOwnPropertyDescriptor(object, sym).enumerable;
				});
			}

			keys.push.apply(keys, symbols);
		}

		return keys;
	}

	function _objectSpread2(target) {
		for (var i = 1; i < arguments.length; i++) {
			var source = arguments[i] != null ? arguments[i] : {};

			if (i % 2) {
				ownKeys(Object(source), true).forEach(function(key) {
					_defineProperty(target, key, source[key]);
				});
			} else if (Object.getOwnPropertyDescriptors) {
				Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
			} else {
				ownKeys(Object(source)).forEach(function(key) {
					Object.defineProperty(
						target,
						key,
						Object.getOwnPropertyDescriptor(source, key)
					);
				});
			}
		}

		return target;
	}

	function _defineProperty(obj, key, value) {
		if (key in obj) {
			Object.defineProperty(obj, key, {
				value: value,
				enumerable: true,
				configurable: true,
				writable: true
			});
		} else {
			obj[key] = value;
		}

		return obj;
	}

	var config = {
		schema: "https",
		host: "a6105722061383-dev.apicloud-saas.com",
		path: "api"
	};

	function req(options) {
		var baseUrl = config.schema + "://" + config.host + "/" + config.path + "/";
		options.url = baseUrl + options.url;
		return new Promise(function(resolve, reject) {
			var Cookie = api.getPrefs({sync: true, key: "COOKIE"});
			if (Cookie) {
				if (options.headers) {
					options.headers.Cookie = Cookie;
				} else {
					options.headers = {Cookie: Cookie};
				}
			}
			api.ajax(options, function(ret, err) {
				console.log(
					"[" +
						options.method +
						"] " +
						options.url +
						" [" +
						api.winName +
						"/" +
						api.frameName +
						"]\n" +
						JSON.stringify(
							_objectSpread2(_objectSpread2({}, options), {}, {ret: ret, err: err})
						)
				);

				if (ret) {
					if (ret.status === 0 || ret.code === 0) {
						resolve(ret.data ? ret.data : ret);
					} else {
						reject(ret);
						if (typeof options.error === "function") {
							options.error(ret);
						} else {
							api.alert({
								title: ret.msg,
								msg: "\u670D\u52A1\u7AEF\u9519\u8BEF\u4EE3\u7801:[" + ret.code + "]"
							});
						}
					}
				} else {
					var errNames = [
						"连接错误",
						"超时",
						"授权错误",
						"数据类型错误",
						"不安全的数据"
					];
					console.log([errNames[err.code], JSON.stringify(err)]);
					reject(err);

					if (typeof options.fail === "function") {
						options.fail(err);
					} else {
						api.toast({
							msg: errNames[err.code] + "[" + err.code + "]\n" + err.msg,
							location: "top",
							duration: 2500
						});
					}
				}
			});
		});
	}

	/**
	 * GET请求快捷方法
	 * @constructor
	 * @param url {string} 地址
	 * @param options {Object} 附加参数
	 */
	function GET(url, options) {
		if (options === void 0) {
			options = {};
		}
		return req(
			_objectSpread2(_objectSpread2({}, options), {}, {url: url, method: "GET"})
		);
	}

	var GoodsAction = /*@__PURE__*/ (function(Component) {
		function GoodsAction(props) {
			Component.call(this, props);
		}

		if (Component) GoodsAction.__proto__ = Component;
		GoodsAction.prototype = Object.create(Component && Component.prototype);
		GoodsAction.prototype.constructor = GoodsAction;
		GoodsAction.prototype.crash = function() {
			var pageParam = {list: [this.props]};
			api.openWin({
				name: "pending_order",
				url: "../../pages/pending_order/pending_order.stml",
				pageParam: pageParam
			});
		};
		GoodsAction.prototype.addCart = function() {
			this.fire("addCart");
		};
		GoodsAction.prototype.render = function() {
			return apivm.h(
				"safe-area",
				{class: "action-bar"},

				apivm.h(
					"view",
					{class: "action-btn btn-add-cart", onClick: this.addCart},
					apivm.h("text", {class: "btn-text"}, "加入预约列表")
				),
				apivm.h(
					"view",
					{class: "action-btn btn-crash", onClick: this.crash},
					apivm.h("text", {class: "btn-text"}, "结算")
				)
			);
		};

		return GoodsAction;
	})(Component);
	GoodsAction.css = {
		".action-bar": {flexDirection: "row", background: "#fff"},
		".btn-add-cart": {background: "#FFC54A", height: "48px"},
		".btn-crash": {background: "#FE5760", height: "48px"},
		".btn-text": {color: "#fff", fontSize: "15px"},
		".action-btn": {flex: "1", alignItems: "center", justifyContent: "center"}
	};
	apivm.define("goods-action", GoodsAction);

	var GoodsCounter = /*@__PURE__*/ (function(Component) {
		function GoodsCounter(props) {
			Component.call(this, props);
		}

		if (Component) GoodsCounter.__proto__ = Component;
		GoodsCounter.prototype = Object.create(Component && Component.prototype);
		GoodsCounter.prototype.constructor = GoodsCounter;
		GoodsCounter.prototype.countChange = function(change) {
			if (this.props.count + change === 0) {
				return api.toast({
					msg: "不能再减少了\n",
					location: "middle"
				});
			}
			this.fire("CountChange", {
				change: change,
				props: this.props
			});
		};
		GoodsCounter.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"view",
				{class: "flex-h"},
				apivm.h("img", {
					class: "num-btn num-decrease",
					src: "../../image/icon/icon-cart-num-decrease.png",
					onClick: function() {
						return this$1.countChange(-1);
					}
				}),
				apivm.h("text", {class: "num-text"}, this.props.count),
				apivm.h("img", {
					class: "num-btn num-increase",
					src: "../../image/icon/icon-cart-num-increase.png",
					onClick: function() {
						return this$1.countChange(1);
					}
				})
			);
		};

		return GoodsCounter;
	})(Component);
	GoodsCounter.css = {
		".flex-h": {
			WebkitBoxOrient: "horizontal",
			WebkitFlexFlow: "row",
			flexFlow: "row"
		},
		".num-btn": {width: "22px", height: "22px", backgroundSize: "22px 22px"},
		".num-text": {
			padding: "0 8px",
			fontSize: "14px",
			color: "#333",
			marginTop: "2px",
			minWidth: "28px",
			textAlign: "center"
		}
	};
	apivm.define("goods-counter", GoodsCounter);

	var GoodsAdd = /*@__PURE__*/ (function(Component) {
		function GoodsAdd(props) {
			Component.call(this, props);
			this.data = {
				background: "rgba(0,0,0,0)",
				goods: false,
				goodsCardBottom: 375,
				count: 1,
				items: []
			};
		}

		if (Component) GoodsAdd.__proto__ = Component;
		GoodsAdd.prototype = Object.create(Component && Component.prototype);
		GoodsAdd.prototype.constructor = GoodsAdd;
		GoodsAdd.prototype.installed = function() {
			var this$1 = this;
			this.data.goods = this.props.goods ? this.props.goods : api.pageParam.goods;
			console.log(this.data.goods);
			this.data.items = api.pageParam.goods.times;
			setTimeout(function() {
				this$1.data.background = "rgba(0,0,0,.5)";
				this$1.data.goodsCardBottom = 0;
			}, 11);
			var cartList = api.getPrefs({sync: true, key: "CART-DATA"});
			if (cartList) {
				cartList = JSON.parse(cartList);
				this.data.cartData = cartList[this.data.goods.id];
				if (this.data.cartData) {
					this.data.count = this.data.cartData.count;
				}
			}
		};
		GoodsAdd.prototype.closeThis = function() {
			this.data.background = "rgba(0,0,0,0)";
			this.data.goodsCardBottom = 375;
			setTimeout(function() {
				if (isMP()) {
					api.sendEvent({
						name: "CLOSE-GOODS-ADD"
					});
				} else {
					api.closeFrame();
				}
			}, 300);
		};
		GoodsAdd.prototype.countChange = function(e) {
			this.data.count += e.detail.change;
		};
		GoodsAdd.prototype.addCart = function() {
			var cartList = api.getPrefs({sync: true, key: "CART-DATA"}) || "{}";
			cartList = JSON.parse(cartList);
			cartList[this.data.goods.id] = {
				goods: this.data.goods,
				count: this.data.count
			};

			api.setPrefs({
				key: "CART-DATA",
				value: cartList
			});

			setTabBarBadge(2, Object.keys(cartList).length);
			api.toast({
				msg: "成功加入" + this.data.count + "个到预约列表",
				location: "middle"
			});

			this.closeThis();
		};
		GoodsAdd.prototype.selectLabel = function(idx) {
			var items = this.data.items;
			for (var i = 0; i < items.length; i++) {
				if (items[i][2] == 1) {
					items[i][2] = 0;
				}
			}
			this.data.items = items;
			this.data.items[idx][2] = 1;
			this.data.goods.curt_price = this.data.items[idx][1];
			this.data.goods.curt_time = this.data.items[idx][0];
		};
		GoodsAdd.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"view",
				{class: "page", style: "background:" + this.data.background},
				apivm.h("view", {class: "mask", onClick: this.closeThis}),
				apivm.h(
					"view",
					{
						class: "move",
						style: "transform:translateY(" + this.data.goodsCardBottom + "px)"
					},
					apivm.h(
						"view",
						{class: "goods-card"},
						apivm.h("img", {
							class: "goods-cover",
							mode: "aspectFill",
							src: this.data.goods.pic_main,
							alt: ""
						}),
						apivm.h(
							"view",
							{class: "goods-info"},
							apivm.h(
								"view",
								{class: "goods-title"},
								apivm.h("text", {class: "goods-title-text"}, this.data.goods.name)
							),
							apivm.h(
								"view",
								{class: "goods-footer"},
								apivm.h("text", {class: "goods-price-signal"}, "¥"),
								apivm.h("text", {class: "goods-price"}, this.data.goods.curt_price),
								apivm.h(GoodsCounter, {
									onCountChange: this.countChange.bind(this),
									count: this.data.count
								})
							),
							apivm.h(
								"view",
								null,
								apivm.h("text", {class: "times-desc"}, "可选择预约以下不同时间段")
							),
							apivm.h(
								"view",
								{class: "select-times"},
								(Array.isArray(this.data.items)
									? this.data.items
									: Object.values(this.data.items)
								).map(function(item$1, index$1) {
									return apivm.h(
										"view",
										{class: "items"},
										apivm.h(
											"text",
											{
												onClick: function() {
													return this$1.selectLabel(index$1);
												},
												style: "border-radius: 10px;padding:4px 6px;",
												class: item$1[2] == 1 ? "active" : "text"
											},
											item$1[0]
										)
									);
								})
							)
						)
					),

					apivm.h(GoodsAction, {
						goods: this.data.goods,
						count: this.data.count,
						onAddCart: function(event) {
							if (this$1.addCart) {
								this$1.addCart(event);
							} else {
								addCart(event);
							}
						}
					})
				)
			);
		};

		return GoodsAdd;
	})(Component);
	GoodsAdd.css = {
		".page": {height: "100%", display: "flex", transition: "all 0.5s"},
		".mask": {flex: "1"},
		".move": {
			position: "relative",
			transition: "all 0.3s",
			transform: "translateY(375px)"
		},
		".goods-card": {
			background: "#fff",
			borderRadius: "10px 10px 0 0",
			padding: "30px 15px",
			flexFlow: "row"
		},
		".goods-cover": {width: "70px", height: "70px"},
		".goods-info": {marginLeft: "10px"},
		".times-desc": {fontSize: "12px", color: "#333", marginBottom: "5px"},
		".select-times": {
			display: "flex",
			justifyContent: "space-between",
			alignItems: "center",
			alignContent: "flex-start",
			flexDirection: "row",
			flexWrap: "wrap",
			alignSelf: "flex-start"
		},
		".items": {marginTop: "15px"},
		".text": {fontSize: "9px", background: "#eee", color: "#333"},
		".active": {fontSize: "9px", background: "#d00", color: "#fff"},
		".active-text": {fontSize: "9px", color: "#fff"},
		".goods-title": {marginBottom: "20px"},
		".goods-title-text": {fontSize: "14px", color: "#333", fontWeight: "bold"},
		".goods-footer": {flexDirection: "row", marginBottom: "20px"},
		".goods-price-signal": {fontSize: "12px", color: "#FE5760", marginTop: "3px"},
		".goods-price": {fontSize: "15px", color: "#FE5760", flex: "1"}
	};
	apivm.define("goods-add", GoodsAdd);

	var MainMenu = /*@__PURE__*/ (function(Component) {
		function MainMenu(props) {
			Component.call(this, props);
			this.data = {
				CD: new Date().getTime(),
				categoryIndex: 0,
				menuList: {},
				scrollTo: 0,
				currentGoods: null
			};
			this.compute = {
				categoryNames: function() {
					return Object.keys(this.data.menuList);
				},
				offsetList: function() {
					var goodsHeight = 90;
					var emptyHeight = 88;
					var goodsNameHeight = 18;
					var offsetList = [0];
					var offset = 0;
					for (var category in this.data.menuList) {
						var goodsLength = this.data.menuList[category].length;
						offset +=
							(goodsLength ? goodsLength * goodsHeight : emptyHeight) +
							goodsNameHeight;
						offsetList.push(offset);
					}
					return offsetList;
				}
			};
		}

		if (Component) MainMenu.__proto__ = Component;
		MainMenu.prototype = Object.create(Component && Component.prototype);
		MainMenu.prototype.constructor = MainMenu;
		MainMenu.prototype.apiready = function() {
			var this$1 = this;
			this.getMenuData();
			api.addEventListener({name: "CLOSE-GOODS-ADD"}, function(_) {
				wx.showTabBar();
				this$1.data.currentGoods = null;
			});
		};
		MainMenu.prototype.switchCategory = function(index) {
			this.data.categoryIndex = index;
			this.data.CD = new Date().getTime() + 500; // 手动切换分类后需要锁定500毫秒 避免右侧scroll-view滚动时带来次生问题
			this.data.scrollTo = this.offsetList[index];
		};
		MainMenu.prototype.onScroll = function(e) {
			var y = isMP() ? e.detail.scrollTop : e.detail.y;

			for (var i = 0; i < this.offsetList.length; i++) {
				if (i) {
					// 第一个不判断
					if (y >= this.offsetList[i - 1] && y < this.offsetList[i]) {
						//判断是否在区间内
						if (new Date().getTime() > this.data.CD) {
							//冷却CD已经不存在
							this.data.categoryIndex = i - 1; //设置左边的高亮
						}
					}
				}
			}
		};
		MainMenu.prototype.openAdd = function(goods) {
			if (isMP()) {
				this.data.currentGoods = goods;
				wx.hideTabBar();
			} else {
				api.openFrame({
					name: "goods_add",
					url: "../goods_add/goods_add.stml",
					pageParam: {goods: goods}
				});
			}
		};
		MainMenu.prototype.getMenuData = function() {
			var this$1 = this;

			GET("goods_categories/goods_list").then(function(data) {
				var menuList = {};
				data.forEach(function(item) {
					menuList[item.name] = item.goods_list;
				});
				this$1.data.menuList = menuList;
			});
		};
		MainMenu.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"view",
				{class: "main-menu"},
				apivm.h(
					"safe-area",
					null,
					apivm.h(
						"view",
						{class: "header"},
						apivm.h("text", {class: "title"}, "场馆")
					)
				),

				apivm.h(
					"view",
					{class: "menu-wrap flex-h flex-1"},
					apivm.h(
						"scroll-view",
						{"scroll-y": true, class: "left-category"},
						(Array.isArray(this.categoryNames)
							? this.categoryNames
							: Object.values(this.categoryNames)
						).map(function(item$1, index$1) {
							return apivm.h(
								"view",
								{
									class:
										this$1.data.categoryIndex === index$1
											? "category-item-current"
											: "category-item-normal",
									onClick: function() {
										return this$1.switchCategory(index$1);
									}
								},
								apivm.h(
									"text",
									{
										class:
											"category-text " +
											(this$1.data.categoryIndex === index$1
												? "category-text-current"
												: "category-text-normal")
									},
									item$1
								)
							);
						})
					),
					apivm.h(
						"scroll-view",
						{
							"scroll-y": true,
							"scroll-with-animation": true,
							class: "right-list flex-1",
							id: "right-list",
							onScroll: this.onScroll,
							"scroll-top": this.data.scrollTo
						},
						(Array.isArray(this.data.menuList)
							? this.data.menuList
							: Object.values(this.data.menuList)
						).map(function(item$1, index$1) {
							return apivm.h(
								"view",
								{id: this$1.categoryNames[index$1]},
								apivm.h(
									"view",
									null,
									apivm.h(
										"text",
										{class: "category-title"},
										this$1.categoryNames[index$1]
									)
								),
								item$1.length
									? apivm.h(
											"view",
											null,
											(Array.isArray(item$1) ? item$1 : Object.values(item$1)).map(
												function(goods) {
													return apivm.h(
														"view",
														{class: "goods-item flex-h"},
														apivm.h("img", {
															class: "goods-pic",
															mode: "aspectFill",
															src: goods.thumbnail,
															alt: ""
														}),
														apivm.h(
															"view",
															{class: "goods-info flex-1"},
															apivm.h("text", {class: "goods-name flex-1"}, goods.name),
															apivm.h(
																"view",
																{class: "flex-h"},
																apivm.h("text", {class: "goods-price-signal"}, "¥"),
																apivm.h(
																	"text",
																	{class: "flex-1 goods-price-num"},
																	goods.curt_price.toFixed(2)
																),
																apivm.h("img", {
																	class: "num-btn num-increase",
																	onClick: function() {
																		return this$1.openAdd(goods);
																	},
																	src: "../../image/icon/icon-cart-num-increase.png"
																})
															)
														)
													);
												}
											)
									  )
									: apivm.h(
											"view",
											{class: "goods-empty flex flex-center"},
											apivm.h("text", {class: "goods-empty-text"}, "当前分类下暂无信息"),
											apivm.h(
												"text",
												{class: "goods-empty-text"},
												"There is no goods in this classification"
											)
									  )
							);
						})
					)
				),

				this.data.currentGoods
					? apivm.h(
							"view",
							{style: "position: absolute;width: 100%;height: 100%;"},
							apivm.h(GoodsAdd, {goods: this.data.currentGoods})
					  )
					: null
			);
		};

		return MainMenu;
	})(Component);
	MainMenu.css = {
		".main-menu": {height: "100%"},
		".title": {
			fontWeight: "500",
			color: "#020202",
			fontSize: "18px",
			textAlign: "center",
			lineHeight: "44px",
			height: "44px"
		},
		".left-category": {width: "110px", background: "#F8F8F8"},
		".category-item-current": {
			background: "#fff",
			height: "50px",
			justifyContent: "center"
		},
		".category-item-normal": {
			background: "#F8F8F8",
			height: "50px",
			justifyContent: "center"
		},
		".category-text": {lineHeight: "50px", textAlign: "center", fontSize: "13px"},
		".category-text-current": {fontWeight: "bold", color: "#333"},
		".category-text-normal": {fontWeight: "normal", color: "#666"},
		".right-list": {padding: "10px"},
		".category-title": {
			height: "18px",
			fontWeight: "500",
			color: "#333333",
			fontSize: "13px",
			lineHeight: "18px"
		},
		".goods-item": {margin: "10px 0"},
		".goods-info": {margin: "0 5px 0 10px"},
		".goods-pic": {width: "70px", height: "70px", borderRadius: "5px"},
		".goods-name": {
			fontSize: "14px",
			lineHeight: "20px",
			fontWeight: "500",
			color: "#333333"
		},
		".goods-price-signal": {
			fontSize: "12px",
			fontWeight: "500",
			color: "#FE5760",
			lineHeight: "22px"
		},
		".goods-price-num": {fontSize: "15px", fontWeight: "500", color: "#FE5760"},
		".goods-empty": {
			height: "68px",
			background: "#f9f9f9",
			margin: "10px",
			borderRadius: "5px"
		},
		".goods-empty-text": {color: "#aaa", fontSize: "11px"},
		".num-btn": {width: "22px", height: "22px"}
	};
	MainMenu.csslink = ["../../css/app.css"];
	apivm.define("main-menu", MainMenu);
	apivm.render(apivm.h("main-menu", null), "body");
})();
