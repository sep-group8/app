(function() {
	var UserLogin = /*@__PURE__*/ (function(Component) {
		function UserLogin(props) {
			Component.call(this, props);
			this.data = {
				username: "",
				password: ""
			};
		}

		if (Component) UserLogin.__proto__ = Component;
		UserLogin.prototype = Object.create(Component && Component.prototype);
		UserLogin.prototype.constructor = UserLogin;
		UserLogin.prototype.apiready = function() {};
		UserLogin.prototype.get_username = function(e) {
			var _this = this;
			_this.data.username = e.detail.value;
		};
		UserLogin.prototype.get_password = function(e) {
			var _this = this;
			_this.data.password = e.detail.value;
		};
		UserLogin.prototype.doLogin = function() {
			var _this = this;
			var username = _this.data.username;
			var password = _this.data.password;
			api.ajax(
				{
					url: "https://a6105722061383-dev.apicloud-saas.com/api/c_users/user_login",
					method: "post",
					headers: {
						"Content-Type": "application/json;charset=utf-8"
					},

					data: {
						body: {
							username: username,
							password: password
						}
					}
				},

				function(ret, err) {
					//正常
					console.log(JSON.stringify(ret));
					if (ret.statusCode == 200) {
						localStorage.setItem("c_user_id", ret.data.id);
						api.setPrefs({
							key: "USER",
							value: JSON.stringify(ret.data)
						});

						api.sendEvent({
							name: "loginSuccess",
							extra: {
								flag: true
							}
						});

						api.toast({
							msg: ret.message,
							duration: 2000,
							location: "bottom"
						});

						setTimeout(function(_) {
							_this.doBack();
						}, 2000);
					} else {
						api.toast({
							msg: ret.message,
							duration: 2000,
							location: "bottom"
						});

						return;
					}
					//异常
					if (err) {
						api.toast({
							msg: "网络异常，请稍后重试",
							duration: 2000,
							location: "bottom"
						});

						return;
					}
				}
			);
		};
		UserLogin.prototype.doBack = function() {
			api.closeWin();
		};
		UserLogin.prototype.userReg = function() {
			api.openWin({
				name: "user_register",
				url: "../user_register/user_register.stml"
			});
		};
		UserLogin.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"scroll-view",
				{class: "page", "scroll-y": true},
				apivm.h("view", {class: "close", onClick: this.doBack}),
				apivm.h(
					"view",
					{class: "login"},
					apivm.h("text", {class: "login-txt"}, "用户登录")
				),
				apivm.h(
					"view",
					{class: "login_form"},
					apivm.h(
						"view",
						{class: "mobile"},
						apivm.h("input", {
							type: "text",
							class: "weui-input",
							placeholder: "请输入登录名",
							"placeholder-style": "color:#fff",
							"adjust-position": "true",
							oninput: function(event) {
								if (this$1.get_username) {
									this$1.get_username(event);
								} else {
									get_username(event);
								}
							}
						})
					),
					apivm.h(
						"view",
						{class: "password"},
						apivm.h("input", {
							type: "password",
							class: "weui-input",
							placeholder: "请输入登录密码",
							"placeholder-style": "color:#fff",
							"adjust-position": "true",
							oninput: function(event) {
								if (this$1.get_password) {
									this$1.get_password(event);
								} else {
									get_password(event);
								}
							}
						})
					),
					apivm.h(
						"view",
						{class: "login-btn", onClick: this.doLogin},
						apivm.h("text", {class: "login-btn-txt"}, "立即登录")
					),
					apivm.h(
						"view",
						{class: "reg-btn", onClick: this.userReg},
						apivm.h("text", {class: "login-btn-txt2"}, "新用户注册")
					)
				)
			);
		};

		return UserLogin;
	})(Component);
	UserLogin.css = {
		".page": {
			backgroundImage: 'url("../../image/bg/bg_body.png")',
			backgroundRepeat: "no-repeat",
			backgroundSize: "100%",
			backgroundPosition: "center top",
			backgroundColor: "#5caaff",
			width: "100%",
			height: "100%"
		},
		".close": {
			width: "95%",
			height: "32px",
			backgroundImage: "url('../../image/icon/close.png')",
			backgroundRepeat: "no-repeat",
			backgroundPosition: "right center",
			backgroundSize: "contain",
			marginTop: "30px"
		},
		".login": {width: "100%", height: "50px", marginTop: "12%"},
		".login-txt": {
			textAlign: "left",
			paddingLeft: "25px",
			fontSize: "30px",
			color: "#fff"
		},
		".login_form": {
			width: "80%",
			height: "auto",
			margin: "20% auto 0",
			boxSizing: "border-box"
		},
		input: {border: "none", background: "transparent"},
		".p-color": {color: "#fff"},
		".weui-input": {
			width: "auto",
			height: "40px",
			color: "#fff",
			fontSize: "20px"
		},
		".mobile": {
			width: "100%",
			height: "40px",
			lineHeight: "40px",
			marginBottom: "20px",
			padding: "0 30px",
			boxSizing: "border-box",
			backgroundImage: 'url("../../image/icon/icon_login_user.png")',
			backgroundRepeat: "no-repeat",
			backgroundPosition: "left center",
			backgroundSize: "16px",
			borderBottom: "1px solid #66beff"
		},
		".password": {
			width: "100%",
			height: "40px",
			lineHeight: "40px",
			marginBottom: "20px",
			padding: "0 30px",
			boxSizing: "border-box",
			backgroundImage: 'url("../../image/icon/icon_login_pass.png")',
			backgroundRepeat: "no-repeat",
			backgroundPosition: "left center",
			backgroundSize: "16px",
			borderBottom: "1px solid #66beff"
		},
		".login-btn": {
			display: "block",
			width: "100%",
			height: "40px",
			boxSizing: "border-box",
			borderRadius: "20px",
			background: "#fff",
			margin: "20px auto",
			textAlign: "center"
		},
		".reg-btn": {
			display: "block",
			width: "100%",
			height: "40px",
			boxSizing: "border-box",
			borderRadius: "20px",
			background: "#66beff",
			margin: "0 auto",
			textAlign: "center"
		},
		".login-btn-txt": {
			lineHeight: "40px",
			fontSize: "20px",
			fontWeight: "bolder",
			color: "#5caaff"
		},
		".login-btn-txt2": {
			lineHeight: "40px",
			fontSize: "20px",
			fontWeight: "bolder",
			color: "#fff"
		}
	};
	UserLogin.csslink = ["../../css/app.css"];
	apivm.define("user_login", UserLogin);
	apivm.render(apivm.h("user_login", null), "body");
})();
