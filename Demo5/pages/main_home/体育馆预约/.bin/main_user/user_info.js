(function() {
	var UserInfo = /*@__PURE__*/ (function(Component) {
		function UserInfo(props) {
			Component.call(this, props);
			this.data = {
				userInfo: {
					nickname: "",
					realname: "",
					sex: "",
					birthday: "",
					long: 0,
					weight: 0,
					sign: ""
				}
			};
		}

		if (Component) UserInfo.__proto__ = Component;
		UserInfo.prototype = Object.create(Component && Component.prototype);
		UserInfo.prototype.constructor = UserInfo;
		UserInfo.prototype.apiready = function() {
			//like created
			var _this = this;
			var uid = localStorage.getItem("c_user_id");
			if (uid != "" || uid != undefined) {
				_this.getUserInfo(uid);
			}
		};
		UserInfo.prototype.getUserInfo = function(id) {
			var _this = this;
			api.ajax(
				{
					url:
						"https://a6105722061383-dev.apicloud-saas.com/api/c_users/getUserInfo",
					method: "post",
					headers: {
						"Content-Type": "application/json;charset=utf-8"
					},

					data: {
						body: {
							id: id
						}
					}
				},

				function(ret, err) {
					//正常
					console.log(JSON.stringify(ret));
					if (ret.statusCode == 200) {
						_this.data.userInfo = ret.data;
					}
					//异常
					if (err) {
						api.toast({
							msg: "网络异常，请稍后重试",
							duration: 2000,
							location: "bottom"
						});

						return;
					}
				}
			);
		};
		UserInfo.prototype.getNickName = function(e) {
			var value = e.detail.value;
			var id = localStorage.getItem("c_user_id");
			var data = {nickname: value};
			this.updateUser(id, data);
			this.data.userInfo.nickname = value;
		};
		UserInfo.prototype.getRealName = function(e) {
			var value = e.detail.value;
			var id = localStorage.getItem("c_user_id");
			var data = {realname: value};
			this.updateUser(id, data);
			this.data.userInfo.realname = value;
		};
		UserInfo.prototype.getSex = function(e) {
			var value = e.detail.value;
			var id = localStorage.getItem("c_user_id");
			var data = {sex: value};
			this.updateUser(id, data);
			this.data.userInfo.sex = value;
		};
		UserInfo.prototype.getBirthday = function(e) {
			var value = e.detail.value;
			var id = localStorage.getItem("c_user_id");
			var data = {birthday: value};
			this.updateUser(id, data);
			this.data.userInfo.birthday = value;
		};
		UserInfo.prototype.getLong = function(e) {
			var value = e.detail.value;
			var id = localStorage.getItem("c_user_id");
			var data = {long: value};
			this.updateUser(id, data);
			this.data.userInfo.long = value;
		};
		UserInfo.prototype.getWeight = function(e) {
			var value = e.detail.value;
			var id = localStorage.getItem("c_user_id");
			var data = {weight: value};
			this.updateUser(id, data);
			this.data.userInfo.weight = value;
		};
		UserInfo.prototype.getSign = function(e) {
			var value = e.detail.value;
			var id = localStorage.getItem("c_user_id");
			var data = {sign: value};
			this.updateUser(id, data);
			this.data.userInfo.sign = value;
		};
		UserInfo.prototype.updateUser = function(id, data) {
			api.ajax(
				{
					url:
						"https://a6105722061383-dev.apicloud-saas.com/api/c_users/updateUserInfo",
					method: "post",
					headers: {
						"Content-Type": "application/json;charset=utf-8"
					},

					data: {
						body: {
							id: id,
							data: data
						}
					}
				},

				function(ret, err) {
					//正常
					if (ret.statusCode == 200) {
						api.toast({
							msg: ret.message,
							duration: 1000,
							location: "bottom"
						});
					}
					//异常
					if (err) {
						api.toast({
							msg: "网络异常，请稍后重试",
							duration: 2000,
							location: "bottom"
						});

						return;
					}
				}
			);
		};
		UserInfo.prototype.doBack = function() {
			api.closeWin();
		};
		UserInfo.prototype.loginOut = function() {
			var _this = this;
			api.confirm(
				{
					title: "退出账号",
					msg: "确定要退出当前账号吗",
					buttons: ["确认退出", "取消"]
				},
				function(ret) {
					var index = ret.buttonIndex;
					if (index == 1) {
						api.removePrefs({
							key: "USER"
						});

						api.removePrefs({
							key: "COOKIE"
						});

						localStorage.clear();
						api.sendEvent({
							name: "loginOut",
							extra: {
								flag: true
							}
						});

						api.toast({
							msg: "退出成功",
							duration: 2000,
							location: "bottom"
						});

						setTimeout(function(_) {
							_this.doBack();
						}, 2000);
					}
				}
			);
		};
		UserInfo.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"view",
				{class: "page flex"},
				apivm.h(
					"safe-area",
					null,
					apivm.h(
						"view",
						{class: "header flex flex-h flex-between"},
						apivm.h("view", {class: "flex-1 back", onClick: this.doBack}),
						apivm.h(
							"view",
							{class: "flex-1"},
							apivm.h("text", {class: "title"}, "个人资料")
						),
						apivm.h("view", {class: "flex-1"})
					)
				),
				apivm.h(
					"scroll-view",
					{class: "form", "scroll-y": true},
					apivm.h(
						"view",
						{class: "form-item flex flex-h flex-left"},
						apivm.h(
							"view",
							{class: "flex-label"},
							apivm.h("text", {class: "label"}, "您的昵称：")
						),
						apivm.h("input", {
							class: "flex-input",
							type: "text",
							placeholder: "给自己取一个昵称吧？",
							onInput: function(e) {
								if (typeof userInfo != "undefined") {
									userInfo.nickname = e.target.value;
								} else {
									this$1.data.userInfo.nickname = e.target.value;
								}
							},
							onblur: function(event) {
								if (this$1.getNickName) {
									this$1.getNickName(event);
								} else {
									getNickName(event);
								}
							},
							"adjust-position": "true",
							value:
								typeof userInfo == "undefined"
									? this.data.userInfo.nickname
									: userInfo.nickname
						})
					),
					apivm.h(
						"view",
						{class: "form-item flex flex-h flex-left"},
						apivm.h(
							"view",
							{class: "flex-label"},
							apivm.h("text", {class: "label"}, "您的姓名：")
						),
						apivm.h("input", {
							class: "flex-input",
							type: "text",
							placeholder: "您的真实姓名是？",
							onInput: function(e) {
								if (typeof userInfo != "undefined") {
									userInfo.realname = e.target.value;
								} else {
									this$1.data.userInfo.realname = e.target.value;
								}
							},
							onblur: function(event) {
								if (this$1.getRealName) {
									this$1.getRealName(event);
								} else {
									getRealName(event);
								}
							},
							"adjust-position": "true",
							value:
								typeof userInfo == "undefined"
									? this.data.userInfo.realname
									: userInfo.realname
						})
					),
					apivm.h(
						"view",
						{class: "form-item flex flex-h flex-left"},
						apivm.h(
							"view",
							{class: "flex-label"},
							apivm.h("text", {class: "label"}, "您的性别：")
						),
						apivm.h("input", {
							class: "flex-input",
							type: "text",
							placeholder: "您的性别是？",
							onInput: function(e) {
								if (typeof userInfo != "undefined") {
									userInfo.sex = e.target.value;
								} else {
									this$1.data.userInfo.sex = e.target.value;
								}
							},
							onblur: function(event) {
								if (this$1.getSex) {
									this$1.getSex(event);
								} else {
									getSex(event);
								}
							},
							"adjust-position": "true",
							value:
								typeof userInfo == "undefined" ? this.data.userInfo.sex : userInfo.sex
						})
					),
					apivm.h(
						"view",
						{class: "form-item flex flex-h flex-left"},
						apivm.h(
							"view",
							{class: "flex-label"},
							apivm.h("text", {class: "label"}, "您的生日：")
						),
						apivm.h("input", {
							class: "flex-input",
							type: "text",
							placeholder: "您的生日是什么时候？",
							onInput: function(e) {
								if (typeof userInfo != "undefined") {
									userInfo.birthday = e.target.value;
								} else {
									this$1.data.userInfo.birthday = e.target.value;
								}
							},
							onblur: function(event) {
								if (this$1.getBirthday) {
									this$1.getBirthday(event);
								} else {
									getBirthday(event);
								}
							},
							"adjust-position": "true",
							value:
								typeof userInfo == "undefined"
									? this.data.userInfo.birthday
									: userInfo.birthday
						})
					),
					apivm.h(
						"view",
						{class: "form-item flex flex-h flex-left"},
						apivm.h(
							"view",
							{class: "flex-label"},
							apivm.h("text", {class: "label"}, "您的身高：")
						),
						apivm.h("input", {
							class: "flex-input",
							style: "width:65%",
							type: "text",
							placeholder: "您的身高是多少？",
							onInput: function(e) {
								if (typeof userInfo != "undefined") {
									userInfo.long = e.target.value;
								} else {
									this$1.data.userInfo.long = e.target.value;
								}
							},
							"keyboard-type": "number",
							onblur: function(event) {
								if (this$1.getLong) {
									this$1.getLong(event);
								} else {
									getLong(event);
								}
							},
							"adjust-position": "true",
							value:
								typeof userInfo == "undefined" ? this.data.userInfo.long : userInfo.long
						}),
						apivm.h(
							"view",
							{class: "flex-desc"},
							apivm.h("text", {class: "desc"}, "cm")
						)
					),
					apivm.h(
						"view",
						{class: "form-item flex flex-h flex-left"},
						apivm.h(
							"view",
							{class: "flex-label"},
							apivm.h("text", {class: "label"}, "您的体重：")
						),
						apivm.h("input", {
							class: "flex-input",
							style: "width:65%",
							type: "text",
							placeholder: "您的体重是多少？",
							onInput: function(e) {
								if (typeof userInfo != "undefined") {
									userInfo.weight = e.target.value;
								} else {
									this$1.data.userInfo.weight = e.target.value;
								}
							},
							"keyboard-type": "number",
							onblur: function(event) {
								if (this$1.getWeight) {
									this$1.getWeight(event);
								} else {
									getWeight(event);
								}
							},
							"adjust-position": "true",
							value:
								typeof userInfo == "undefined"
									? this.data.userInfo.weight
									: userInfo.weight
						}),
						apivm.h(
							"view",
							{class: "flex-desc"},
							apivm.h("text", {class: "desc"}, "kg")
						)
					),
					apivm.h(
						"view",
						{class: "form-item flex flex-h flex-left"},
						apivm.h(
							"view",
							{class: "flex-label"},
							apivm.h("text", {class: "label"}, "个性签名：")
						),
						apivm.h("input", {
							class: "flex-input",
							type: "text",
							placeholder: "留下您的个性签名吧？",
							onInput: function(e) {
								if (typeof userInfo != "undefined") {
									userInfo.sign = e.target.value;
								} else {
									this$1.data.userInfo.sign = e.target.value;
								}
							},
							onblur: function(event) {
								if (this$1.getSign) {
									this$1.getSign(event);
								} else {
									getSign(event);
								}
							},
							"adjust-position": "true",
							value:
								typeof userInfo == "undefined" ? this.data.userInfo.sign : userInfo.sign
						})
					),
					apivm.h(
						"view",
						{class: "login-out-btn", onClick: this.loginOut},
						apivm.h("text", {class: "login-out-text"}, "退出登录")
					)
				)
			);
		};

		return UserInfo;
	})(Component);
	UserInfo.css = {
		".page": {height: "100%"},
		".header": {padding: "0 10px", boxSizing: "border-box"},
		".title": {
			fontWeight: "500",
			color: "#020202",
			fontSize: "18px",
			textAlign: "center",
			height: "44px",
			lineHeight: "44px"
		},
		".back": {
			height: "44px",
			backgroundImage: "url('../../image/icon/icon-header-back.png')",
			backgroundPosition: "left center",
			backgroundRepeat: "no-repeat",
			backgroundSize: "24px"
		},
		".form-item": {
			padding: "5px 15px",
			borderBottom: "1px solid #eee",
			boxSizing: "border-box"
		},
		".flex-label": {height: "40px"},
		".label": {
			height: "40px",
			lineHeight: "40px",
			fontSize: "16px",
			color: "#666"
		},
		".flex-input": {border: "none", background: "transparent", height: "40px"},
		".login-out-btn": {
			width: "80%",
			height: "40px",
			borderRadius: "20px",
			backgroundColor: "#66beff",
			margin: "30px auto",
			textAlign: "center"
		},
		".login-out-text": {
			height: "40px",
			lineHeight: "40px",
			fontSize: "18px",
			fontWeight: "500",
			textAlign: "center",
			color: "#fff"
		}
	};
	UserInfo.csslink = ["../../css/app.css"];
	apivm.define("user_info", UserInfo);
	apivm.render(apivm.h("user_info", null), "body");
})();
