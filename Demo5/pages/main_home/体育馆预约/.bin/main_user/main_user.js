(function() {
	function ownKeys(object, enumerableOnly) {
		var keys = Object.keys(object);

		if (Object.getOwnPropertySymbols) {
			var symbols = Object.getOwnPropertySymbols(object);

			if (enumerableOnly) {
				symbols = symbols.filter(function(sym) {
					return Object.getOwnPropertyDescriptor(object, sym).enumerable;
				});
			}

			keys.push.apply(keys, symbols);
		}

		return keys;
	}

	function _objectSpread2(target) {
		for (var i = 1; i < arguments.length; i++) {
			var source = arguments[i] != null ? arguments[i] : {};

			if (i % 2) {
				ownKeys(Object(source), true).forEach(function(key) {
					_defineProperty(target, key, source[key]);
				});
			} else if (Object.getOwnPropertyDescriptors) {
				Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
			} else {
				ownKeys(Object(source)).forEach(function(key) {
					Object.defineProperty(
						target,
						key,
						Object.getOwnPropertyDescriptor(source, key)
					);
				});
			}
		}

		return target;
	}

	function _defineProperty(obj, key, value) {
		if (key in obj) {
			Object.defineProperty(obj, key, {
				value: value,
				enumerable: true,
				configurable: true,
				writable: true
			});
		} else {
			obj[key] = value;
		}

		return obj;
	}

	var config = {
		schema: "https",
		host: "a6105722061383-dev.apicloud-saas.com",
		path: "api"
	};

	function req(options) {
		var baseUrl = config.schema + "://" + config.host + "/" + config.path + "/";
		options.url = baseUrl + options.url;
		return new Promise(function(resolve, reject) {
			var Cookie = api.getPrefs({sync: true, key: "COOKIE"});
			if (Cookie) {
				if (options.headers) {
					options.headers.Cookie = Cookie;
				} else {
					options.headers = {Cookie: Cookie};
				}
			}
			api.ajax(options, function(ret, err) {
				console.log(
					"[" +
						options.method +
						"] " +
						options.url +
						" [" +
						api.winName +
						"/" +
						api.frameName +
						"]\n" +
						JSON.stringify(
							_objectSpread2(_objectSpread2({}, options), {}, {ret: ret, err: err})
						)
				);

				if (ret) {
					if (ret.status === 0 || ret.code === 0) {
						resolve(ret.data ? ret.data : ret);
					} else {
						reject(ret);
						if (typeof options.error === "function") {
							options.error(ret);
						} else {
							api.alert({
								title: ret.msg,
								msg: "\u670D\u52A1\u7AEF\u9519\u8BEF\u4EE3\u7801:[" + ret.code + "]"
							});
						}
					}
				} else {
					var errNames = [
						"连接错误",
						"超时",
						"授权错误",
						"数据类型错误",
						"不安全的数据"
					];
					console.log([errNames[err.code], JSON.stringify(err)]);
					reject(err);

					if (typeof options.fail === "function") {
						options.fail(err);
					} else {
						api.toast({
							msg: errNames[err.code] + "[" + err.code + "]\n" + err.msg,
							location: "top",
							duration: 2500
						});
					}
				}
			});
		});
	}

	/**
	 * POST 请求快捷方法
	 * @param url
	 * @param data
	 * @param options {Object} 附加参数
	 * @returns {Promise<Object>}
	 * @constructor
	 */
	function POST(url, data, options) {
		if (options === void 0) {
			options = {};
		}
		return req(
			_objectSpread2(
				_objectSpread2({}, options),
				{},
				{
					url: url,
					method: "POST",
					data: {
						values: data
					}
				}
			)
		);
	}

	/**
	 * 判断当前是否是小程序环境
	 * 2020-11-24  暂用 api.uiMode模拟
	 * 后期需修正为  api.platform
	 * @returns {boolean}
	 */
	function isMP() {
		return !api.uiMode;
	}

	/**
	 * 获取用户信息
	 * @returns {boolean|any}
	 */
	function getUser() {
		var user = api.getPrefs({
			sync: true,
			key: "USER"
		});

		if (user) {
			return JSON.parse(user);
		}
		return false;
	}

	var OrderItem = /*@__PURE__*/ (function(Component) {
		function OrderItem(props) {
			Component.call(this, props);
		}

		if (Component) OrderItem.__proto__ = Component;
		OrderItem.prototype = Object.create(Component && Component.prototype);
		OrderItem.prototype.constructor = OrderItem;
		OrderItem.prototype.action = function(name) {
			this.fire("OrderAction", {name: name, props: this.props});
		};
		OrderItem.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"view",
				{class: "order-item-order-item"},
				apivm.h(
					"view",
					{class: "order-item-order-header"},
					this.props.order.status === 2
						? apivm.h(
								"view",
								{class: "order-item-order-num"},
								apivm.h("text", {class: "order-item-order-num-key"}, "排队号"),
								apivm.h(
									"text",
									{class: "order-item-order-num-value"},
									this.props.order.meal_num
								)
						  )
						: apivm.h("text", {class: "order-item-order-status"}, "订单已完成")
				),

				apivm.h(
					"view",
					{class: "order-item-order-goods"},
					(Array.isArray(this.props.order.toorderdetails)
						? this.props.order.toorderdetails
						: Object.values(this.props.order.toorderdetails)
					).map(function(goods) {
						return apivm.h(
							"view",
							{class: "order-item-order-goods-item flex-h"},
							apivm.h("img", {
								class: "order-item-goods-cover",
								src: goods.togoodsdetails.thumbnail,
								alt: ""
							}),
							apivm.h(
								"text",
								{class: "order-item-goods-title flex-1"},
								goods.togoodsdetails.name
							),
							apivm.h("text", {class: "order-item-goods-count"}, "× ", goods.goods_num)
						);
					})
				),

				apivm.h(
					"view",
					{class: "order-item-order-price flex-h"},
					apivm.h(
						"text",
						{class: "order-item-order-price-text  flex-1"},
						"商品总价"
					),
					apivm.h(
						"text",
						{class: "order-item-order-price-text"},
						"¥ ",
						this.props.order.total
					)
				),

				apivm.h(
					"view",
					{class: "order-item-order-info"},
					apivm.h(
						"view",
						{class: "order-item-order-info-item flex-h"},
						apivm.h("text", {class: "order-item-order-info-key"}, "下单时间"),
						apivm.h(
							"text",
							{class: "order-item-order-info-value"},
							this.props.order.createAt
						)
					),
					apivm.h(
						"view",
						{class: "order-item-order-info-item flex-h"},
						apivm.h("text", {class: "order-item-order-info-key"}, "完成状态"),
						this.props.order.status === 2
							? apivm.h("text", {class: "order-item-order-info-value"}, "待完成")
							: this.props.order.status === 5
							? apivm.h("text", {class: "order-item-order-info-value"}, "已取消")
							: apivm.h("text", {class: "order-item-order-info-value"}, "已完成")
					),
					apivm.h(
						"view",
						{class: "order-item-order-info-item flex-h"},
						apivm.h("text", {class: "order-item-order-info-key"}, "排队号"),
						apivm.h(
							"text",
							{class: "order-item-order-info-value"},
							this.props.order.meal_num
						)
					),
					apivm.h(
						"view",
						{class: "order-item-order-info-item flex-h"},
						apivm.h("text", {class: "order-item-order-info-key"}, "备注"),
						apivm.h(
							"text",
							{class: "order-item-order-info-value"},
							this.props.order.remark
						)
					)
				),

				this.props.order.status === 2
					? apivm.h(
							"view",
							{class: "order-item-order-action"},
							apivm.h(
								"view",
								{
									class: "order-item-act-btn act-btn-red",
									onClick: function() {
										return this$1.action("orders/cancel");
									}
								},
								apivm.h("text", {class: "order-item-act-text act-text-red"}, "取消订单")
							)
					  )
					: this.props.order.status === 4
					? apivm.h(
							"view",
							{class: "order-item-order-action"},
							apivm.h(
								"text",
								{class: "order-item-act-text-btn act-text act-text-gray"},
								"等待商家审核"
							),
							apivm.h(
								"view",
								{
									class: "order-item-act-btn act-btn-red",
									onClick: function() {
										return this$1.action("orders/withdraw");
									}
								},
								apivm.h("text", {class: "order-item-act-text act-text-red"}, "取消申请")
							)
					  )
					: apivm.h(
							"view",
							{class: "order-item-order-action"},
							apivm.h("view", null),
							apivm.h(
								"view",
								{class: "order-item-act-btn act-btn-red"},
								apivm.h("text", {class: "order-item-act-text act-text-red"}, "删除订单")
							)
					  )
			);
		};

		return OrderItem;
	})(Component);
	OrderItem.css = {
		".order-item-order-item": {
			border: "1px solid #eee",
			margin: "0 15px 10px",
			padding: "10px 10px 0 10px",
			borderRadius: "5px",
			display: "flex",
			flexDirection: "column"
		},
		".order-item-order-header": {
			display: "flex",
			alignItems: "center",
			paddingBottom: "10px"
		},
		".order-status": {
			fontSize: "24px",
			fontWeight: "bold",
			color: "#333",
			margin: "10px",
			textAlign: "center"
		},
		".order-item-order-num": {
			display: "flex",
			justifyContent: "center",
			alignItems: "center"
		},
		".order-item-order-num-key": {
			margin: "5px",
			color: "#333",
			fontWeight: "bold",
			fontSize: "15px"
		},
		".order-item-order-goods": {
			borderTop: "1px solid #EEE",
			borderBottom: "1px solid #EEE",
			paddingBottom: "10px"
		},
		".order-item-order-goods-item": {marginTop: "10px", alignItems: "center"},
		".order-item-goods-title": {
			marginLeft: "10px",
			fontSize: "15px",
			fontWeight: "bold",
			color: "#333"
		},
		".order-item-goods-cover": {
			width: "54px",
			height: "54px",
			borderRadius: "5px"
		},
		".order-item-order-price": {
			padding: "15px 0",
			borderBottom: "1px solid #EEE"
		},
		".order-item-order-price-text": {
			color: "#333",
			fontWeight: "bold",
			fontSize: "15px"
		},
		".flex-h": {flexFlow: "row"},
		".flex-1": {flex: "1"},
		".order-item-order-num-value": {
			color: "#FE5760",
			fontSize: "24px",
			fontWeight: "bold"
		},
		".order-item-order-info": {
			paddingBottom: "10px",
			borderBottom: "1px solid #EEE"
		},
		".order-item-order-info-item": {marginTop: "10px"},
		".order-item-order-info-key": {
			fontSize: "12px",
			color: "#666",
			width: "60px"
		},
		".order-item-order-info-value": {fontSize: "12px", color: "#666"},
		".order-item-order-action": {
			height: "54px",
			flexFlow: "row",
			alignItems: "center",
			justifyContent: "flex-end"
		},
		".order-item-act-btn": {
			padding: "7px 20px",
			borderRadius: "4px",
			marginLeft: "10px",
			height: "34px"
		},
		".order-item-act-text-btn": {marginLeft: "10px"},
		".act-text": {fontSize: "15px", whiteSpace: "nowrap"},
		".act-btn-red": {border: "1px solid #FE5760"},
		".act-text-red": {color: "#FE5760"},
		".act-btn-gray": {border: "1px solid #999"},
		".act-text-gray": {color: "#999"}
	};
	apivm.define("order-item", OrderItem);

	var EmptyBlock = /*@__PURE__*/ (function(Component) {
		function EmptyBlock(props) {
			Component.call(this, props);
		}

		if (Component) EmptyBlock.__proto__ = Component;
		EmptyBlock.prototype = Object.create(Component && Component.prototype);
		EmptyBlock.prototype.constructor = EmptyBlock;
		EmptyBlock.prototype.render = function() {
			return apivm.h(
				"view",
				{class: "empty-block-empty-block"},
				apivm.h("img", {
					class: "empty-block-empty-image",
					src: "../../image/empty/bg-empty-" + this.props.type + ".png",
					alt: ""
				}),
				apivm.h("text", {class: "empty-block-tips-text"}, this.props.text)
			);
		};

		return EmptyBlock;
	})(Component);
	EmptyBlock.css = {
		".empty-block-empty-block": {
			display: "flex",
			alignItems: "center",
			justifyContent: "center",
			textAlign: "center"
		},
		".empty-block-empty-image": {width: "285px", height: "186px"},
		".empty-block-tips-text": {color: "#666", fontSize: "18px"}
	};
	apivm.define("empty-block", EmptyBlock);

	var MainUser = /*@__PURE__*/ (function(Component) {
		function MainUser(props) {
			Component.call(this, props);
			this.data = {
				orderList: [],
				loading: false,
				userInfo: getUser()
			};
		}

		if (Component) MainUser.__proto__ = Component;
		MainUser.prototype = Object.create(Component && Component.prototype);
		MainUser.prototype.constructor = MainUser;
		MainUser.prototype.apiready = function() {
			var _this = this;
			//登录成功监听
			api.addEventListener(
				{
					name: "loginSuccess"
				},
				function(ret, err) {
					var uid = localStorage.getItem("c_user_id");
					if (uid != "" || uid != undefined) {
						_this.getUserInfo(uid);
					}
				}
			);
			//退出登录监听
			api.addEventListener(
				{
					name: "loginOut"
				},
				function(ret, err) {
					_this.logout();
				}
			);
			//下单成功监听
			api.addEventListener(
				{
					name: "PAY-SUCCESS"
				},
				function(_) {
					_this.getOrderList();
					if (isMP()) {
						api.sendEvent({
							name: "CLOSE-GOODS-ADD"
						});
					} else {
						setTimeout(function(_) {
							api.closeFrame({
								name: "goods_add"
							});
						}, 345);
					}
				}
			);
			_this.getOrderList();
		};
		MainUser.prototype.onRefresh = function() {
			var this$1 = this;

			var _this = this;
			_this.data.loading = true;
			if (_this.data.userInfo) {
				_this.getOrderList();
			} else {
				setTimeout(function(_) {
					this$1.data.loading = false;
					api.toast({
						msg: "请登录后查看历史订单"
					});
				}, 1000);
			}
		};
		MainUser.prototype.userLogin = function() {
			api.openWin({
				name: "user_login",
				url: "../user_login/user_login.stml"
			});
		};
		MainUser.prototype.userInfo = function() {
			api.openWin({
				name: "user_info",
				url: "./user_info.stml"
			});
		};
		MainUser.prototype.getUserInfo = function(id) {
			var _this = this;
			api.ajax(
				{
					url:
						"https://a6105722061383-dev.apicloud-saas.com/api/c_users/getUserInfo",
					method: "post",
					headers: {
						"Content-Type": "application/json;charset=utf-8"
					},

					data: {
						body: {
							id: id
						}
					}
				},

				function(ret, err) {
					//正常
					console.log(JSON.stringify(ret));
					if (ret.statusCode == 200) {
						_this.data.userInfo = ret.data;
						_this.getOrderList();
					} else {
						_this.data.userInfo = false;
						_this.data.orderList = [];
					}
					//异常
					if (err) {
						api.toast({
							msg: "网络异常，请稍后重试",
							duration: 2000,
							location: "bottom"
						});

						return;
					}
				}
			);
		};
		MainUser.prototype.logout = function() {
			this.data.userInfo = false;
			this.data.orderList = [];
		};
		MainUser.prototype.getOrderList = function() {
			var this$1 = this;

			var uid = localStorage.getItem("c_user_id");
			POST("orders/get_all_user_list", {uid: uid}).then(function(data) {
				this$1.data.orderList = data;
				setTimeout(function() {
					this$1.data.loading = false;
				}, 1000);
			});
		};
		MainUser.prototype.orderAction = function(e) {
			var this$1 = this;

			var ref = e.detail;
			var name = ref.name;
			var order_id = ref.props.order.id;
			POST(name, {order_id: order_id}).then(function(res) {
				api.toast(res);
				this$1.getOrderList();
			});
		};
		MainUser.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"view",
				{class: "main-user flex"},
				apivm.h(
					"safe-area",
					null,
					apivm.h(
						"view",
						{class: "header"},
						apivm.h("text", {class: "title"}, "我的")
					)
				),
				this.data.userInfo
					? apivm.h(
							"view",
							{class: "user-info flex flex-h flex-center-v", onClick: this.userInfo},
							apivm.h("img", {
								class: "user-avatar",
								src: "../../image/icon/icon-user-avatar.png",
								alt: ""
							}),
							apivm.h(
								"view",
								{class: "flex flex-v lex-left"},
								apivm.h("text", {class: "user-name"}, this.data.userInfo.username),
								apivm.h(
									"view",
									{class: "flex flex-h flex-center-v"},
									apivm.h(
										"text",
										{class: "user-desc", style: "margin-left:10px"},
										"性别:",
										this.data.userInfo.sex
									),
									apivm.h(
										"text",
										{class: "user-desc", style: "margin-left:5px"},
										"生日:",
										this.data.userInfo.birthday
									),
									apivm.h(
										"text",
										{class: "user-desc", style: "margin-left:5px"},
										"身高:",
										this.data.userInfo.long,
										"cm"
									),
									apivm.h(
										"text",
										{class: "user-desc", style: "margin-left:5px"},
										"体重:",
										this.data.userInfo.weight,
										"kg"
									)
								)
							)
					  )
					: apivm.h(
							"view",
							{class: "user-info flex flex-h flex-center-v", onClick: this.userLogin},
							apivm.h("img", {
								class: "user-avatar",
								src: "../../image/icon/icon-user-avatar.png",
								alt: ""
							}),
							apivm.h("text", {class: "user-name"}, "用户登录")
					  ),

				apivm.h(
					"scroll-view",
					{
						"scroll-y": true,
						class: "flex-1 main-user-scroll-view",
						"enable-back-to-top": true,
						"refresher-enabled": true,
						"refresher-triggered": this.data.loading,
						onRefresherrefresh: this.onRefresh
					},
					this.data.orderList.length
						? apivm.h(
								"view",
								null,
								(Array.isArray(this.data.orderList)
									? this.data.orderList
									: Object.values(this.data.orderList)
								).map(function(order) {
									return apivm.h(OrderItem, {
										order: order,
										onOrderAction: this$1.orderAction.bind(this$1)
									});
								})
						  )
						: apivm.h(
								"view",
								{class: "empty-block"},
								apivm.h("empty-block", {text: "暂无订单哦～", type: "order"})
						  )
				)
			);
		};

		return MainUser;
	})(Component);
	MainUser.css = {
		".main-user": {height: "100%"},
		".title": {
			fontWeight: "500",
			color: "#020202",
			fontSize: "18px",
			textAlign: "center",
			height: "44px",
			lineHeight: "44px"
		},
		".user-info": {padding: "15px 0 15px 15px", borderBottom: "1px solid #eee"},
		".user-desc": {fontSize: "12px", color: "#666"},
		".user-avatar": {width: "54px", height: "54px", borderRadius: "50%"},
		".user-name": {
			fontSize: "20px",
			fontWeight: "bold",
			color: "#333333",
			marginLeft: "10px",
			marginBottom: "10px"
		},
		".empty-block": {marginTop: "50px"}
	};
	MainUser.csslink = ["../../css/app.css"];
	apivm.define("main-user", MainUser);
	apivm.render(apivm.h("main-user", null), "body");
})();
