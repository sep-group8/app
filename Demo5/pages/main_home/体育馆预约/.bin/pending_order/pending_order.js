(function() {
	/**
	 * 判断当前是否是小程序环境
	 * 2020-11-24  暂用 api.uiMode模拟
	 * 后期需修正为  api.platform
	 * @returns {boolean}
	 */
	function isMP() {
		return !api.uiMode;
	}

	/**
	 * 统一设置底部tabBar的数字小红点
	 * @param index
	 * @param text
	 */
	function setTabBarBadge(index, text) {
		text += "";
		if (isMP()) {
			if (text === "0") {
				wx.removeTabBarBadge({index: index});
			} else {
				wx.setTabBarBadge({index: index, text: text});
			}
		} else {
			api.setTabBarItemAttr({index: index, badge: {text: text, color: "#FFF"}});
		}
	}

	function ownKeys(object, enumerableOnly) {
		var keys = Object.keys(object);

		if (Object.getOwnPropertySymbols) {
			var symbols = Object.getOwnPropertySymbols(object);

			if (enumerableOnly) {
				symbols = symbols.filter(function(sym) {
					return Object.getOwnPropertyDescriptor(object, sym).enumerable;
				});
			}

			keys.push.apply(keys, symbols);
		}

		return keys;
	}

	function _objectSpread2(target) {
		for (var i = 1; i < arguments.length; i++) {
			var source = arguments[i] != null ? arguments[i] : {};

			if (i % 2) {
				ownKeys(Object(source), true).forEach(function(key) {
					_defineProperty(target, key, source[key]);
				});
			} else if (Object.getOwnPropertyDescriptors) {
				Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
			} else {
				ownKeys(Object(source)).forEach(function(key) {
					Object.defineProperty(
						target,
						key,
						Object.getOwnPropertyDescriptor(source, key)
					);
				});
			}
		}

		return target;
	}

	function _defineProperty(obj, key, value) {
		if (key in obj) {
			Object.defineProperty(obj, key, {
				value: value,
				enumerable: true,
				configurable: true,
				writable: true
			});
		} else {
			obj[key] = value;
		}

		return obj;
	}

	var config = {
		schema: "https",
		host: "a6105722061383-dev.apicloud-saas.com",
		path: "api"
	};

	function req(options) {
		var baseUrl = config.schema + "://" + config.host + "/" + config.path + "/";
		options.url = baseUrl + options.url;
		return new Promise(function(resolve, reject) {
			var Cookie = api.getPrefs({sync: true, key: "COOKIE"});
			if (Cookie) {
				if (options.headers) {
					options.headers.Cookie = Cookie;
				} else {
					options.headers = {Cookie: Cookie};
				}
			}
			api.ajax(options, function(ret, err) {
				console.log(
					"[" +
						options.method +
						"] " +
						options.url +
						" [" +
						api.winName +
						"/" +
						api.frameName +
						"]\n" +
						JSON.stringify(
							_objectSpread2(_objectSpread2({}, options), {}, {ret: ret, err: err})
						)
				);

				if (ret) {
					if (ret.status === 0 || ret.code === 0) {
						resolve(ret.data ? ret.data : ret);
					} else {
						reject(ret);
						if (typeof options.error === "function") {
							options.error(ret);
						} else {
							api.alert({
								title: ret.msg,
								msg: "\u670D\u52A1\u7AEF\u9519\u8BEF\u4EE3\u7801:[" + ret.code + "]"
							});
						}
					}
				} else {
					var errNames = [
						"连接错误",
						"超时",
						"授权错误",
						"数据类型错误",
						"不安全的数据"
					];
					console.log([errNames[err.code], JSON.stringify(err)]);
					reject(err);

					if (typeof options.fail === "function") {
						options.fail(err);
					} else {
						api.toast({
							msg: errNames[err.code] + "[" + err.code + "]\n" + err.msg,
							location: "top",
							duration: 2500
						});
					}
				}
			});
		});
	}

	/**
	 * POST 请求快捷方法
	 * @param url
	 * @param data
	 * @param options {Object} 附加参数
	 * @returns {Promise<Object>}
	 * @constructor
	 */
	function POST(url, data, options) {
		if (options === void 0) {
			options = {};
		}
		return req(
			_objectSpread2(
				_objectSpread2({}, options),
				{},
				{
					url: url,
					method: "POST",
					data: {
						values: data
					}
				}
			)
		);
	}

	var PendingOrder = /*@__PURE__*/ (function(Component) {
		function PendingOrder(props) {
			Component.call(this, props);
			this.data = {
				list: [],
				remark: ""
			};
			this.compute = {
				totalPrice: function() {
					var list = this.data.list;
					return (list.length
						? list.reduce(function(total, item) {
								return total + item.goods.curt_price * item.count;
						  }, 0)
						: 0
					).toFixed(2);
				},
				formData: function() {
					var uid = localStorage.getItem("c_user_id");
					var remark = this.data.remark;
					var list = this.data.list.map(function(item) {
						return {
							goods_id: item.goods.id,
							num: item.count,
							curt_price: item.goods.curt_price,
							curt_time: item.goods.curt_time,
							uid: uid
						};
					});
					return {remark: remark, list: list};
				},
				goods_id: function() {},
				num: function() {},
				curt_price: function() {},
				curt_time: function() {},
				uid: function() {},
				remark: function() {},
				list: function() {}
			};
		}

		if (Component) PendingOrder.__proto__ = Component;
		PendingOrder.prototype = Object.create(Component && Component.prototype);
		PendingOrder.prototype.constructor = PendingOrder;
		PendingOrder.prototype.apiready = function() {
			this.data.list = api.pageParam.list;
		};
		PendingOrder.prototype.close = function() {
			api.closeWin();
		};
		PendingOrder.prototype.onBlur = function(e) {
			this.data.remark = e.target.value;
		};
		PendingOrder.prototype.addOrder = function() {
			POST("orders/app_addorder", this.formData).then(function(data) {
				// 打开结果页
				api.openWin({
					name: "pay_result",
					url: "../pay_result/pay_result.stml"
				});

				// 通知支付成功 刷新订单页面
				api.sendEvent({
					name: "PAY-SUCCESS"
				});

				// 清空购物车
				api.setPrefs({
					key: "CART-DATA",
					value: {}
				});

				setTabBarBadge(2, 0);
			});
		};
		PendingOrder.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"safe-area",
				{class: "page"},

				apivm.h(
					"view",
					{class: "header"},
					apivm.h("img", {
						src: "../../image/icon/icon-header-back.png",
						alt: "",
						class: "header-icon",
						onClick: this.close
					}),
					apivm.h("text", {class: "header-text"}, "待付款订单"),
					apivm.h("view", {class: "header-icon"})
				),

				apivm.h(
					"scroll-view",
					{class: "list"},
					apivm.h("text", {class: "list-title"}, "商品列表"),
					(Array.isArray(this.data.list)
						? this.data.list
						: Object.values(this.data.list)
					).map(function(item$1) {
						return apivm.h(
							"view",
							{class: "list-item"},
							apivm.h("img", {
								class: "goods-cover",
								mode: "aspectFill",
								src: item$1.goods.thumbnail,
								alt: ""
							}),
							apivm.h(
								"view",
								{class: "goods-info"},
								apivm.h(
									"view",
									{class: "goods-main"},
									apivm.h("text", {class: "goods-title"}, item$1.goods.name),
									apivm.h("text", {class: "goods-price-signal"}, "¥"),
									apivm.h(
										"text",
										{class: "goods-price"},
										item$1.goods.curt_price.toFixed(2)
									)
								),
								apivm.h("text", {class: "goods-count"}, "× ", item$1.count),
								apivm.h(
									"view",
									{class: "goods-select"},
									apivm.h("text", {class: "goods-select-title"}, "预约时间"),
									apivm.h("text", {class: "goods-select-time"}, item$1.goods.curt_time)
								)
							)
						);
					}),
					apivm.h(
						"view",
						{class: "order-note"},
						apivm.h("text", {class: "order-note-key"}, "备注"),
						apivm.h("input", {
							class: "order-note-input",
							placeholder: "如需备注请输入",
							onBlur: function(event) {
								if (this$1.onBlur) {
									this$1.onBlur(event);
								} else {
									onBlur(event);
								}
							},
							maxlength: "30",
							id: "remark"
						})
					)
				),

				apivm.h(
					"view",
					{class: "order-footer"},
					apivm.h("text", {class: "total-text"}, "合计："),
					apivm.h("text", {class: "total-price"}, "¥ ", this.totalPrice),
					apivm.h(
						"view",
						{class: "order-pay-btn", onClick: this.addOrder},
						apivm.h("text", {class: "order-pay-btn-text"}, "立即支付")
					)
				)
			);
		};

		return PendingOrder;
	})(Component);
	PendingOrder.css = {
		".page": {height: "100%", display: "flex"},
		".header": {
			height: "44px",
			alignItems: "center",
			justifyContent: "space-between",
			flexFlow: "row"
		},
		".header-icon": {width: "22px", height: "22px", margin: "10px"},
		".header-text": {color: "#020202", fontSize: "18px", fontWeight: "bold"},
		".list": {flex: "1", padding: "15px", boxSizing: "border-box"},
		".list-title": {fontSize: "18px", fontWeight: "bold", color: "#666"},
		".list-item": {marginTop: "20px", flexFlow: "row"},
		".goods-cover": {width: "70px", height: "70px", borderRadius: "4px"},
		".goods-info": {display: "flex", marginLeft: "10px", flex: "1"},
		".goods-main": {flexFlow: "row"},
		".goods-title": {
			fontSize: "15px",
			color: "#333",
			fontWeight: "bold",
			flex: "1"
		},
		".goods-price-signal": {
			color: "#FE5760",
			fontWeight: "bold",
			fontSize: "14px",
			marginTop: "3px"
		},
		".goods-price": {color: "#FE5760", fontWeight: "bold", fontSize: "18px"},
		".goods-sub": {flex: "1", justifyContent: "center", alignItems: "flex-end"},
		".goods-count": {
			fontSize: "15px",
			color: "#999",
			display: "block",
			textAlign: "right"
		},
		".goods-select": {
			display: "flex",
			justifyContent: "space-between",
			flexDirection: "row",
			flexWrap: "nowrap",
			alignItems: "center"
		},
		".goods-select-title": {
			fontSize: "12px",
			lineHeight: "20px",
			color: "#aaa",
			textAlign: "left",
			flex: "1"
		},
		".goods-select-time": {
			fontSize: "12px",
			lineHeight: "20px",
			color: "#aaa",
			textAlign: "right",
			flex: "1"
		},
		".order-note": {flexFlow: "row", padding: "15px 0", alignItems: "center"},
		".order-note-key": {fontSize: "15px", color: "#333", fontWeight: "bold"},
		".order-note-input": {
			flex: "1",
			textAlign: "right",
			fontSize: "15px",
			border: "none"
		},
		".order-footer": {
			display: "flex",
			flexDirection: "row",
			alignItems: "center",
			borderTop: "1px solid #eee"
		},
		".total-text": {color: "#333", fontSize: "12px", marginLeft: "15px"},
		".total-price": {
			color: "#FE5760",
			fontSize: "20px",
			fontWeight: "bold",
			flex: "1"
		},
		".order-pay-btn": {
			width: "120px",
			height: "48px",
			background: "#FE5760",
			alignItems: "center",
			justifyContent: "center"
		},
		".order-pay-btn-text": {color: "#fff", fontSize: "15px"}
	};
	apivm.define("pending_order", PendingOrder);
	apivm.render(apivm.h("pending_order", null), "body");
})();
