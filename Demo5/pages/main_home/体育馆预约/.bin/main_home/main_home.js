(function() {
	/**
	 * 2020-11-24  暂用 api.uiMode模拟
	 * 后期需修正为  api.platform
	 * @returns {boolean}
	 */
	function isMP() {
		return !api.uiMode;
	}

	/**
	 * 统一设置底部tabBar的数字小红点
	 * @param index
	 * @param text
	 */
	function setTabBarBadge(index, text) {
		text += "";
		if (isMP()) {
			if (text === "0") {
				wx.removeTabBarBadge({index: index});
			} else {
				wx.setTabBarBadge({index: index, text: text});
			}
		} else {
			api.setTabBarItemAttr({index: index, badge: {text: text, color: "#FFF"}});
		}
	}

	function ownKeys(object, enumerableOnly) {
		var keys = Object.keys(object);

		if (Object.getOwnPropertySymbols) {
			var symbols = Object.getOwnPropertySymbols(object);

			if (enumerableOnly) {
				symbols = symbols.filter(function(sym) {
					return Object.getOwnPropertyDescriptor(object, sym).enumerable;
				});
			}

			keys.push.apply(keys, symbols);
		}

		return keys;
	}

	function _objectSpread2(target) {
		for (var i = 1; i < arguments.length; i++) {
			var source = arguments[i] != null ? arguments[i] : {};

			if (i % 2) {
				ownKeys(Object(source), true).forEach(function(key) {
					_defineProperty(target, key, source[key]);
				});
			} else if (Object.getOwnPropertyDescriptors) {
				Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
			} else {
				ownKeys(Object(source)).forEach(function(key) {
					Object.defineProperty(
						target,
						key,
						Object.getOwnPropertyDescriptor(source, key)
					);
				});
			}
		}

		return target;
	}

	function _defineProperty(obj, key, value) {
		if (key in obj) {
			Object.defineProperty(obj, key, {
				value: value,
				enumerable: true,
				configurable: true,
				writable: true
			});
		} else {
			obj[key] = value;
		}

		return obj;
	}

	var config = {
		schema: "https",
		host: "a6105722061383-dev.apicloud-saas.com",
		path: "api"
	};

	function req(options) {
		var baseUrl = config.schema + "://" + config.host + "/" + config.path + "/";
		options.url = baseUrl + options.url;
		return new Promise(function(resolve, reject) {
			var Cookie = api.getPrefs({sync: true, key: "COOKIE"});
			if (Cookie) {
				if (options.headers) {
					options.headers.Cookie = Cookie;
				} else {
					options.headers = {Cookie: Cookie};
				}
			}
			api.ajax(options, function(ret, err) {
				console.log(
					"[" +
						options.method +
						"] " +
						options.url +
						" [" +
						api.winName +
						"/" +
						api.frameName +
						"]\n" +
						JSON.stringify(
							_objectSpread2(_objectSpread2({}, options), {}, {ret: ret, err: err})
						)
				);

				if (ret) {
					if (ret.status === 0 || ret.code === 0) {
						resolve(ret.data ? ret.data : ret);
					} else {
						reject(ret);
						if (typeof options.error === "function") {
							options.error(ret);
						} else {
							api.alert({
								title: ret.msg,
								msg: "\u670D\u52A1\u7AEF\u9519\u8BEF\u4EE3\u7801:[" + ret.code + "]"
							});
						}
					}
				} else {
					var errNames = [
						"连接错误",
						"超时",
						"授权错误",
						"数据类型错误",
						"不安全的数据"
					];
					console.log([errNames[err.code], JSON.stringify(err)]);
					reject(err);

					if (typeof options.fail === "function") {
						options.fail(err);
					} else {
						api.toast({
							msg: errNames[err.code] + "[" + err.code + "]\n" + err.msg,
							location: "top",
							duration: 2500
						});
					}
				}
			});
		});
	}

	/**
	 * GET请求快捷方法
	 * @constructor
	 * @param url {string} 地址
	 * @param options {Object} 附加参数
	 */
	function GET(url, options) {
		if (options === void 0) {
			options = {};
		}
		return req(
			_objectSpread2(_objectSpread2({}, options), {}, {url: url, method: "GET"})
		);
	}

	var GoodsListItem = /*@__PURE__*/ (function(Component) {
		function GoodsListItem(props) {
			Component.call(this, props);
		}

		if (Component) GoodsListItem.__proto__ = Component;
		GoodsListItem.prototype = Object.create(Component && Component.prototype);
		GoodsListItem.prototype.constructor = GoodsListItem;
		GoodsListItem.prototype.intoGoodsDetail = function(item) {
			api.openWin({
				name: "goods_detail",
				url: "../../pages/goods_detail/goods_detail.stml",
				pageParam: {
					item: item
				}
			});
		};
		GoodsListItem.prototype.render = function() {
			var this$1 = this;
			return apivm.h(
				"view",
				{class: "goods-list-item-home-goods"},
				apivm.h("text", {class: "goods-list-item-goods-channel"}, this.props.title),
				apivm.h(
					"view",
					{class: "goods-list-item-goods-list"},
					(Array.isArray(this.props.list)
						? this.props.list
						: Object.values(this.props.list)
					).map(function(item$1) {
						return apivm.h(
							"view",
							{
								class: "goods-list-item-goods-item",
								onClick: function() {
									return this$1.intoGoodsDetail(item$1);
								}
							},
							apivm.h("img", {
								class: "goods-list-item-goods-cover",
								src: item$1.togoods.pic_main,
								alt: ""
							}),
							apivm.h(
								"view",
								{class: "goods-list-item-goods-info"},
								apivm.h(
									"text",
									{class: "goods-list-item-goods-name"},
									item$1.togoods.name
								)
							),
							apivm.h(
								"view",
								{class: "goods-list-item-goods-info flex-h flex-center-v"},
								apivm.h(
									"text",
									{class: "goods-list-item-goods-price"},
									"¥ ",
									item$1.togoods.curt_price.toFixed(2)
								),
								apivm.h(
									"text",
									{class: "goods-list-item-goods-price-deleted"},
									"¥ ",
									item$1.togoods.orig_price.toFixed(2)
								)
							)
						);
					})
				)
			);
		};

		return GoodsListItem;
	})(Component);
	GoodsListItem.css = {
		".goods-list-item-home-goods": {background: "#fff", padding: "10px 0"},
		".goods-list-item-goods-channel": {
			fontSize: "18px",
			fontWeight: "500",
			padding: "9px 15px"
		},
		".goods-list-item-goods-list": {
			display: "flex",
			flexWrap: "wrap",
			flexDirection: "row",
			padding: "7px"
		},
		".goods-list-item-goods-item": {
			boxSizing: "border-box",
			width: "50%",
			padding: "0 8px",
			marginBottom: "10px"
		},
		".goods-list-item-goods-cover": {
			width: "100%",
			height: "110px",
			borderRadius: "5px",
			marginBottom: "5px"
		},
		".goods-list-item-goods-info": {
			height: "20px",
			lineHeight: "20px",
			display: "block"
		},
		".goods-list-item-goods-name, .goods-list-item-goods-price": {
			fontSize: "14px",
			color: "#333333",
			fontWeight: "500",
			marginRight: "8px"
		},
		".goods-list-item-goods-price-deleted": {color: "#999", fontSize: "12px"}
	};
	apivm.define("goods-list-item", GoodsListItem);

	var MainHome = /*@__PURE__*/ (function(Component) {
		function MainHome(props) {
			Component.call(this, props);
			this.data = {
				photoHeight: 210,
				opacity: 0,
				shopInfo: {
					shopName: "体育馆",
					address: "四川省 成都市 武侯区 文教园F座",
					operation: {
						status: 1,
						times: ["09:00 - 13:00", "14:00 - 22:00"]
					}
				},

				safeAreaTop: 0,
				classifyList: []
			};
			this.compute = {
				photoRealHeight: function() {
					return this.data.photoHeight + this.data.safeAreaTop;
				},
				statusBarStyle: function() {
					return this.data.opacity < 0.5 ? "light" : "dark";
				}
			};
		}

		if (Component) MainHome.__proto__ = Component;
		MainHome.prototype = Object.create(Component && Component.prototype);
		MainHome.prototype.constructor = MainHome;
		MainHome.prototype.apiready = function() {
			this.data.safeAreaTop = api.safeArea ? api.safeArea.top : 0;
			this.initEventListener();
			this.getData();
			this.getClassifiesData();
			this.getCartNum(); // 初始状态栏是白色的
			api.setStatusBarStyle && api.setStatusBarStyle({style: "light", color: "-"});
		};
		MainHome.prototype.onshow = function() {
			var cartData = api.getPrefs({sync: true, key: "CART-DATA"});
			if (cartData) {
				cartData = JSON.parse(cartData);
				setTabBarBadge(2, Object.keys(cartData).length);
			}
		};
		MainHome.prototype.onScroll = function(e) {
			var y = isMP() ? e.detail.scrollTop : e.detail.y;

			var threshold = this.photoRealHeight - y;
			if (threshold < 0) {
				threshold = 0;
			}
			this.data.opacity = 1 - threshold / this.photoRealHeight;
			api.setStatusBarStyle &&
				api.setStatusBarStyle({
					style: this.statusBarStyle
				});
		};
		MainHome.prototype.initEventListener = function() {
			var this$1 = this;

			// 监听来自其他页面逻辑主动触发的切换tabBar请求，后续需要手动调用setTabIndex来切换
			api.addEventListener(
				{
					name: "SET-TAB-INDEX"
				},
				function(ret) {
					console.log("收到了");
					this$1.setTabIndex(ret.value);
				}
			);

			// 监听来自tabBar本身点击触发的切换tabBar请求，后续需要手动调用setTabIndex来切换
			api.addEventListener(
				{
					name: "tabitembtn"
				},
				this.setTabIndex
			);
		};
		MainHome.prototype.setTabIndex = function(ret) {
			var index = ret.index;
			api.setTabBarAttr({index: index}); // 切换tabBar
			api.setStatusBarStyle &&
				api.setStatusBarStyle({
					// 设置状态栏
					style: index ? "dark" : this.statusBarStyle
				});
		};
		MainHome.prototype.getCartNum = function() {
			var cartList = api.getPrefs({sync: true, key: "CART-DATA"});
			if (cartList) {
				cartList = JSON.parse(cartList);
				setTabBarBadge(2, Object.keys(cartList).length);
			}
		};
		MainHome.prototype.getData = function(format) {
			var this$1 = this;

			//修改删除了shops的s
			GET("shop/getInfo").then(function(data) {
				console.log("////", data);
				this$1.data.shopInfo = data;
			});
		};
		MainHome.prototype.callPhone = function() {
			if (isMP()) {
				wx.makePhoneCall({
					phoneNumber: this.data.shopInfo.phone
				});
			} else {
				api.call({
					type: "tel_prompt",
					number: this.data.shopInfo.phone
				});
			}
		};
		MainHome.prototype.getClassifiesData = function() {
			var this$1 = this;

			GET("classifies/getClassifyGoodsList").then(function(data) {
				console.log(data);
				this$1.data.classifyList = data;
			});
		};
		MainHome.prototype.render = function() {
			return apivm.h(
				"view",
				{class: "main-home"},
				apivm.h("img", {
					class: "shop-photo",
					style: "height:" + this.photoRealHeight + "px",
					src: this.data.shopInfo.img,
					alt: ""
				}),

				apivm.h(
					"scroll-view",
					{
						"scroll-y": true,
						class: "flex-1 scroll-view",
						"enable-back-to-top": true,
						onScroll: this.onScroll
					},
					apivm.h(
						"view",
						{class: "shop", style: "margin-top:" + this.photoRealHeight + "px"},
						apivm.h(
							"view",
							{class: "shop-header flex-h"},
							apivm.h(
								"text",
								{class: "shop-name flex-1 ellipsis-1"},
								this.data.shopInfo.name
							),
							apivm.h("img", {
								class: "shop-phone",
								onClick: this.callPhone,
								src: "../../image/icon/icon-home-phone.png",
								alt: ""
							})
						),
						apivm.h(
							"view",
							{class: "content-wrap"},
							apivm.h(
								"text",
								{class: "shop-text shop-address"},
								this.data.shopInfo.city,
								" ",
								this.data.shopInfo.country,
								" ",
								this.data.shopInfo.address
							)
						),
						apivm.h(
							"view",
							{class: "shop-operation content-wrap"},
							apivm.h(
								"text",
								{class: "shop-text"},
								"营业中 ",
								this.data.shopInfo.business_hours
							)
						)
					),

					(Array.isArray(this.data.classifyList)
						? this.data.classifyList
						: Object.values(this.data.classifyList)
					).map(function(item$1) {
						return apivm.h(
							"view",
							{class: "list"},
							apivm.h("goods-list-item", {
								class: "goods-item",
								list: item$1.togc,
								title: item$1.name
							})
						);
					})
				),
				apivm.h(
					"view",
					{
						class: "header-bar",
						style:
							"opacity:" +
							this.data.opacity +
							";padding-top:" +
							this.data.safeAreaTop +
							"px"
					},
					apivm.h("text", {class: "nav-title shop-name"}, this.data.shopInfo.name)
				)
			);
		};

		return MainHome;
	})(Component);
	MainHome.css = {
		".main-home": {
			height: "100%",
			background: "#F6F2F2",
			position: "relative",
			display: "flex"
		},
		".content-wrap": {padding: "10px 15px"},
		".header-bar": {
			position: "absolute",
			width: "100%",
			zIndex: "999",
			background: "#f9f9f9"
		},
		".nav-title": {height: "44px", lineHeight: "44px", textAlign: "center"},
		".scroll-view": {marginTop: "-11px"},
		".shop": {background: "#fff", borderRadius: "10px 10px 0 0"},
		".shop-photo": {position: "absolute", width: "100%"},
		".shop-header": {padding: "10px 15px 0"},
		".shop-name": {fontSize: "18px", fontWeight: "500"},
		".shop-phone": {width: "22px", height: "22px"},
		".shop-operation": {borderTop: "1px solid #eee"},
		".shop-text": {color: "#666", fontSize: "15px"},
		".shop-address": {width: "80%"},
		".list": {paddingTop: "10px", background: "#F6F2F2"}
	};
	MainHome.csslink = ["../../css/app.css"];
	apivm.define("main-home", MainHome);
	apivm.render(apivm.h("main-home", null), "body");
})();
