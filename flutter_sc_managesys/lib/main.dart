import 'package:flutter_sc_managesys/pages/welcome/welcome.dart';
import 'package:flutter_sc_managesys/routes.dart';
import 'package:flutter/material.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      routes: staticRoutes,
      home: WelcomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}