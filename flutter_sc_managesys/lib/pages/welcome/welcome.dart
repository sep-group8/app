import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../common/utils/utils.dart';
import '../../common/values/values.dart';

class WelcomePage extends StatefulWidget {
  WelcomePage({Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  // 标题
  Widget _buildPageHeadTitle() {
    return Container(
      margin: EdgeInsets.only(top: duSetHeight(60)),
      child: Text(
        "Features",
        textAlign: TextAlign.center,
        style: TextStyle(
          color: AppColors.primaryText,
          fontFamily: "Montserrat",
          fontWeight: FontWeight.w600,
          fontSize: duSetFontSize(24),
        ),
      ),
    );
  }

  // 说明
  Widget _buildPageHeadDetail() {
    return Container(
      width: duSetWidth(242),
      height: duSetHeight(70),
      margin: EdgeInsets.only(top: duSetHeight(14)),
      child: Text(
        "Here are the details",
        textAlign: TextAlign.center,
        style: TextStyle(
          color: AppColors.primaryText,
          fontFamily: "Avenir",
          fontWeight: FontWeight.normal,
          fontSize: duSetFontSize(16),
          height: 1.2,
        ),
      ),
    );
  }

  // 特性
  Widget _buildFeatureItem(String imageName, String intro, double marginTop) {
    return Container(
      width: duSetWidth(295),
      height: duSetHeight(80),
      margin: EdgeInsets.only(top: duSetHeight(marginTop)),
      child: Row(
        children: [
          Container(
            width: duSetWidth(80),
            height: duSetWidth(80),
            child: Image.asset(
              "assets/images/$imageName.png",
              fit: BoxFit.none,
            ),
          ),
          Spacer(),
          Container(
            width: duSetWidth(195),
            child: Text(
              intro,
              textAlign: TextAlign.left,
              style: TextStyle(
                color: AppColors.primaryText,
                fontFamily: "Avenir",
                fontWeight: FontWeight.normal,
                fontSize: duSetFontSize(16),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // 按钮
  Widget _buildStartButton() {
    return Container(
      width: duSetWidth(295),
      height: duSetHeight(44),
      margin: EdgeInsets.only(bottom: duSetHeight(20)),
      // child: FlatButton(
      //   color: AppColors.primaryElement,
      //   textColor: AppColors.primaryElementText,
      //   child: Text("Get started"),
      //   shape: RoundedRectangleBorder(
      //     borderRadius: Radii.k6pxRadius,
      //   ),
      //   onPressed: () {
      //     Navigator.pushNamed(
      //       context,
      //       "/sign-in",
      //     );
      //   },
      // ),
      child: TextButton(
        autofocus: true,
        style: ButtonStyle(
            textStyle: MaterialStateProperty.all(TextStyle(fontSize: 24)),
            foregroundColor: MaterialStateProperty.resolveWith(
              (states) {
                if (states.contains(MaterialState.focused) &&
                    !states.contains(MaterialState.pressed)) {
                  //获取焦点时的颜色
                  return Colors.blue;
                } else if (states.contains(MaterialState.pressed)) {
                  //按下时的颜色
                  return Colors.deepPurple;
                }
                //默认状态使用灰色
                return Colors.grey;
              },
            ),
            //背景颜色
            backgroundColor: MaterialStateProperty.resolveWith((states) {
              //设置按下时的背景颜色
              if (states.contains(MaterialState.pressed)) {
                return Colors.blue[200];
              }
              //默认不使用背景颜色
              return null;
            }),
            //设置水波纹颜色
            overlayColor: MaterialStateProperty.all(Colors.lightBlue[50]),
            side: MaterialStateProperty.all(
                const BorderSide(color: Colors.blue, width: 2)),
            shape: MaterialStateProperty.all(RoundedRectangleBorder(
                borderRadius:
                    BorderRadius.all(Radius.circular(duSetWidth(6)))))),
        child: Text('Get Started'),
        onPressed: () {
          Navigator.pushNamed(context, "/sign-in");
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //高度去掉，顶部 底部 导航
    ScreenUtil.init(
        BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width, //屏幕宽度
          maxHeight: MediaQuery.of(context).size.height, //屏幕高度
        ),
        designSize: const Size(375, 812 - 44 - 34), // 设计图尺寸
        orientation: Orientation.portrait,
        context: context, // 初始化context
        minTextAdapt: true);

    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Material App Bar'),
      // ),
      body: Center(
        child: Container(
          child: Column(
            children: <Widget>[
              _buildPageHeadTitle(),
              _buildPageHeadDetail(),
              _buildFeatureItem("feature-1", "test1", 86),
              _buildFeatureItem("feature-2", "test2", 40),
              _buildFeatureItem("feature-3", "test3", 40),
              const Spacer(),
              _buildStartButton()
            ],
          ),
        ),
      ),
    );
  }
}
